#Antes de correr hacer source envars.sh 
set -ex
PGM1=letkf.exe
PGM2=efso.exe
PGM3=mean.exe
PGM4=obsope.exe
F90=mpiifort
#OMP='-qopenmp'
OMP=''
FFLAG='-assume underscore'
F90OPT='-O3 -xHost ' #-convert big_endian' #-O3 -convert big_endian' #-Kfast,parallel' # -Hs'
#F90OPT='-O0 -g' #-convert big_endian' #-O3 -convert big_endian' #-Kfast,parallel' # -Hs'


BLAS=0 #0: no blas 1: using blas
BASEDIR=/share/Libs/  # directorio base de la libreria BLAS

rm -f *.mod
rm -f *.o

COMMONDIR=../../common/

cat $COMMONDIR/netlib.f > netlib2.f
if test $BLAS -eq 1
then
LBLAS="-SSL2BLAMP"
LSCALAPACK="-SCALAPACK"
INCLUDE= #"-I${BASEDIR}/EigenExa-2.3c/"
Leigen= #"-L${BASEDIR}/EigenExa-2.3c/ -lEigenExa"

else
cat $COMMONDIR/netlibblas.f >> netlib2.f
LBLAS=""
fi

LIB_NETCDF="-L$NETCDF/lib/ -lnetcdff -lnetcdf"
INC_NETCDF="-I$NETCDF/include/"


COMMONDIR=../../common/
ln -fs $COMMONDIR/SFMT.f90 ./
ln -fs $COMMONDIR/common.f90 ./
ln -fs $COMMONDIR/common_mpi.f90 ./
ln -fs $COMMONDIR/common_mtx.f90 ./
ln -fs $COMMONDIR/common_letkf.f90 ./
ln -fs $COMMONDIR/common_smooth2d.f90 ./

ln -fs ../common/common_wrf.f90 ./
ln -fs ../common/common_mpi_wrf.f90 ./
ln -fs ../common/common_obs_wrf.f90 ./
ln -fs ../common/module_map_utils.f90 ./
ln -fs ../common/common_namelist.f90 ./

# estos serian los common modules del makefile:
$F90 $OMP $F90OPT -c SFMT.f90
$F90 $OMP $F90OPT -c common.f90
$F90 $OMP $F90OPT -c common_mpi.f90
$F90 $OMP $F90OPT -c common_mtx.f90
$F90 $OMP $F90OPT -c netlib2.f
$F90 $OMP $F90OPT -c common_letkf.f90
$F90 $OMP $F90OPT -c module_map_utils.f90
$F90 $OMP $F90OPT $INC_NETCDF -c common_wrf.f90
$F90 $OMP $F90OPT $INC_NETCDF -c common_namelist.f90
$F90 $OMP $F90OPT -c common_smooth2d.f90
$F90 $OMP $F90OPT $INC_NETCDF -c common_obs_wrf.f90
$F90 $OMP $F90OPT $INC_NETCDF -c common_mpi_wrf.f90

COMMON_DIR=./
COMMON_OBJS="${COMMON_DIR}/SFMT.o           \
              ${COMMON_DIR}/common.o         \
              ${COMMON_DIR}/common_mpi.o     \
              ${COMMON_DIR}/common_mtx.o     \
              ${COMMON_DIR}/netlib2.o         \
              ${COMMON_DIR}/common_letkf.o   \
              ${COMMON_DIR}/module_map_utils.o   \
              ${COMMON_DIR}/common_wrf.o     \
              ${COMMON_DIR}/common_namelist.o     \
              ${COMMON_DIR}/common_smooth2d.o     \
              ${COMMON_DIR}/common_obs_wrf.o \
              ${COMMON_DIR}/common_mpi_wrf.o"

# generamos los objs del letkf y efso:
$F90 $OMP $F90OPT $INC_NETCDF -c letkf_obs.f90
$F90 $OMP $F90OPT -c efso_nml.f90
$F90 $OMP $F90OPT $INC_NETCDF -c efso_tools.f90
$F90 $OMP $F90OPT $INC_NETCDF -c letkf_tools.f90
OBJS="letkf_obs.o letkf_tools.o efso_tools.o efso_nml.o"

# generamos los .o de los tres main que necesitamos:
$F90 $OMP $F90OPT $INC_NETCDF -c letkf.f90
$F90 $OMP $F90OPT $INC_NETCDF -c efso.f90
$F90 $OMP $F90OPT $INC_NETCDF -c mean.f90
$F90 $OMP $F90OPT $INC_NETCDF -c obsope.f90

# compilamos los tres main:
$F90 $OMP $F90OPT -o ${PGM1} ${COMMON_OBJS} ${OBJS} letkf.o $LBLAS $LSCALAPACK $LIB_NETCDF
$F90 $OMP $F90OPT -o ${PGM2} ${COMMON_OBJS} ${OBJS} efso.o $LBLAS $LSCALAPACK $LIB_NETCDF
$F90 $OMP $F90OPT -o ${PGM3} ${COMMON_OBJS} letkf_obs.o mean.o $LBLAS $LSCALAPACK $LIB_NETCDF
$F90 $OMP $F90OPT -o ${PGM4} ${COMMON_OBJS} obsope.o $LBLAS $LSCALAPACK $LIB_NETCDF

rm -f *.mod
rm -f *.o
rm -f netlib2.f

rm SFMT.f90
rm common.f90
rm common_mpi.f90
rm common_mtx.f90
rm common_letkf.f90
rm common_wrf.f90
rm common_mpi_wrf.f90
rm common_obs_wrf.f90
rm module_map_utils.f90
rm common_smooth2d.f90
rm common_namelist.f90


cd ../wrf_to_wps

PGM=merge_wrfinput.exe

FLAGS="-O2"


$F90 $FLAGS $INC_NETCDF -c merge_wrfinput.f90
$F90 $FLAGS -o ${PGM} *.o $LIB_NETCDF

rm *.o

echo "NORMAL END"
