MODULE letkf_tools
!=======================================================================
!
! [PURPOSE:] Module for LETKF with WRF
!
! [HISTORY:]
!   01/26/2009 Takemasa Miyoshi  created
!
!=======================================================================
!$USE OMP_LIB
  USE common
  USE common_mpi
  USE common_wrf
  USE common_mpi_wrf
  USE common_letkf
  USE letkf_obs
  USE efso_nml
  USE efso_tools

  IMPLICIT NONE

  PRIVATE
  PUBLIC ::  das_efso,das_letkf,gues3d,gues2d,anal3d,anal2d,guesp2d,analp2d

  INTEGER,SAVE :: nobstotal
  INTEGER,SAVE :: var_local_n2n(nv3d+nv2d)
  INTEGER,SAVE :: var_local_par_n2n(np2d)
  INTEGER,SAVE :: var_local_par_n2n_0d(np2d)
  REAL(r_size),ALLOCATABLE :: gues3d(:,:,:,:) ! background ensemble
  REAL(r_size),ALLOCATABLE :: gues2d(:,:,:)   !  output: destroyed
  REAL(r_size),ALLOCATABLE :: guesp2d(:,:,:)  !Parameters
  REAL(r_size),ALLOCATABLE :: anal3d(:,:,:,:) ! analysis ensemble
  REAL(r_size),ALLOCATABLE :: anal2d(:,:,:)   !
  REAL(r_size),ALLOCATABLE :: analp2d(:,:,:)  !Parameters
  INTEGER      , ALLOCATABLE :: np2dtop0d(:)
  INTEGER :: iworkp0d,np0d
  REAL(r_size), PARAMETER :: q_sprd_max = 0.5 !GYL, maximum q (ensemble spread)/(ensemble mean)
CONTAINS

!-----------------------------------------------------------------------
! Data Assimilation
!-----------------------------------------------------------------------
SUBROUTINE das_letkf
  IMPLICIT NONE
  CHARACTER(12) :: inflfile='infl_mul.grd'
  REAL(r_size),ALLOCATABLE :: hdxf(:,:)
  REAL(r_size),ALLOCATABLE :: rdiag(:)
  REAL(r_size),ALLOCATABLE :: rloc(:)
  REAL(r_size),ALLOCATABLE :: dep(:)
  REAL(r_size),ALLOCATABLE :: work3d(:,:,:)
  REAL(r_size),ALLOCATABLE :: work2d(:,:)
  REAL(r_size),ALLOCATABLE :: workp2d(:,:)
  REAL(r_sngl),ALLOCATABLE :: workg(:,:,:)
  REAL(r_size),ALLOCATABLE :: work3da(:,:,:)     !GYL
  REAL(r_size),ALLOCATABLE :: work2da(:,:)       !GYL
  REAL(r_size),ALLOCATABLE :: logpfm(:,:),zfm(:,:)
  REAL(r_size) :: parm
  REAL(r_size) :: trans(nbv,nbv,nv3d+nv2d)
  REAL(r_size) :: trans_p(nbv,nbv,np2d)
  REAL(r_size) :: transm(nbv,nv3d+nv2d)    !GYL
  REAL(r_size) :: transrlx(nbv,nbv)        !GYL
  REAL(r_size) :: pa(nbv,nbv,nv3d+nv2d)    !GYL
  REAL(r_size) :: q_mean,q_sprd            !GYL
  REAL(r_size) :: q_anal(nbv)              !GYL

  LOGICAL :: ex
  INTEGER :: ij,ilev,n,m,i,j,k,nobsl,ierr,iv,iolen,irec
  INTEGER :: counterp
  INTEGER :: npoints

  REAL(r_size),ALLOCATABLE :: mean3dg(:,:,:),mean3da(:,:,:)
  REAL(r_size),ALLOCATABLE :: mean2dg(:,:),mean2da(:,:)
  REAL(r_size),ALLOCATABLE :: meanp2d(:,:)
  REAL(r_size),ALLOCATABLE :: sprdp2d(:,:)
  REAL(r_size),ALLOCATABLE :: work3dg(:,:,:,:)  !G
  REAL(r_size),ALLOCATABLE :: work2dg(:,:,:)    !G

!  REAL(r_size)             :: tmpsprd

  WRITE(6,'(A)') 'Hello from das_letkf'
  WRITE(6,'(A,F15.2)') '  cov_infl_mul = ',cov_infl_mul, '  relax_alpha = ' &
                 ,relax_alpha , ' relax_alpha_spread =', relax_alpha_spread
  nobstotal = nobs !+ ntvs comentado en SMN 
  WRITE(6,'(A,I8)') 'Target observation numbers : NOBS=',nobs!,', NTVS=',ntvs comentado en SMN
  !
  ! In case of no obs
  !
  !IF(nobstotal == 0) THEN
  !  WRITE(6,'(A)') 'No observation assimilated'
    !anal3d = gues3d
    !anal2d = gues2d
    !IF(ESTPAR)analp2d= guesp2d
    !RETURN
  !END IF

  !
  ! INITIALIZE PARAMETERS (FIRST CYCLE ONLY)
  !

  IF(ESTPAR)THEN
    CALL init_par_mpi(guesp2d,nbv,update_parameter_2d,update_parameter_0d,param_default_value,param_sprd_init)
  ENDIF


  !
  ! Variable localization
  !
  var_local_n2n(1) = 1
  DO n=2,nv3d+nv2d
    DO i=1,n
      var_local_n2n(n) = i
      IF(MAXVAL(ABS(var_local(i,:)-var_local(n,:))) < TINY(var_local)) EXIT
    END DO
  END DO
  IF(ESTPAR)THEN
  var_local_par_n2n(1) = 1
  DO n=2,np2d
    DO i=1,n
      var_local_par_n2n(n) = i
      IF(MAXVAL(ABS(var_local_par(i,:)-var_local_par(n,:))) < TINY(var_local_par) &
         .AND. update_parameter_2d(i) == 1 ) EXIT
    END DO
  END DO
  ENDIF
!print *,var_local_n2n
!print *,var_local_par_n2n
  !
  ! FCST PERTURBATIONS
  !
  ALLOCATE(mean3dg(nij1,nlev,nv3d),mean2dg(nij1,nv2d))
  ALLOCATE(mean3da(nij1,nlev,nv3d),mean2da(nij1,nv2d))
  IF(ESTPAR)ALLOCATE(meanp2d(nij1,np2d))
  CALL ensmean_grd(nbv,nij1,gues3d,gues2d,mean3dg,mean2dg)
  IF(ESTPAR)CALL ensmean_ngrd(nbv,nij1,guesp2d,meanp2d,np2d)
  DO n=1,nv3d
    DO m=1,nbv
      DO k=1,nlev
        DO i=1,nij1
          gues3d(i,k,m,n) = gues3d(i,k,m,n) - mean3dg(i,k,n)
        END DO
      END DO
    END DO
  END DO
  DO n=1,nv2d
    DO m=1,nbv
      DO i=1,nij1
        gues2d(i,m,n) = gues2d(i,m,n) - mean2dg(i,n)
      END DO
    END DO
  END DO


  !
  ! multiplicative inflation
  !
  IF(cov_infl_mul > 0.0d0) THEN ! fixed multiplicative inflation parameter
    ALLOCATE( work3d(nij1,nlev,nv3d) )
    ALLOCATE( work2d(nij1,nv2d) )
    work3d = cov_infl_mul
    work2d = cov_infl_mul
  END IF
  IF(cov_infl_mul <= 0.0d0) THEN ! 3D parameter values are read-in
    ALLOCATE( workg(nlon,nlat,nlev) )
    ! para la escritura del obsanalNNN.dat
    ALLOCATE( work3dg(nlon,nlat,nlev,nv3d) ) ! esto es lo que yo necesito
    ALLOCATE( work2dg(nlon,nlat,nv2d) ) ! esto es lo que yo necesito
    ALLOCATE( work3d(nij1,nlev,nv3d) )
    ALLOCATE( work2d(nij1,nv2d) )
    IF(myrank == 0) THEN
      INQUIRE(FILE=inflfile,EXIST=ex)
      IF(ex) THEN
        WRITE(6,'(A,I3.3,2A)') 'MYRANK ',myrank,' is reading.. ',inflfile
        INQUIRE(IOLENGTH=iolen) iolen
        OPEN(55,FILE=inflfile,FORM='unformatted',ACCESS='direct',RECL=nlon*nlat*iolen)
        !OPEN(55,FILE=inflfile,FORM='unformatted',ACCESS='sequential')
      ELSE   
        work3dg = -1.0d0 * cov_infl_mul
        work2dg = -1.0d0 * cov_infl_mul
      END IF
    END IF
    irec=1
    DO iv=1,nv3d
      DO ilev=1,nlev
        IF(myrank ==0)THEN
          IF(ex)THEN
            READ(55,rec=irec)workg(:,:,ilev)
            irec=irec+1
          ELSE
            workg(:,:,ilev)=-1.0d0 * cov_infl_mul
          ENDIF
        ENDIF
       ENDDO

       CALL scatter_ngrd_mpi(0,workg,work3d(:,:,iv),nlev)
    ENDDO
    DO iv=1,nv2d
        IF(myrank ==0)THEN
          IF(ex)THEN
            READ(55,rec=irec)workg(:,:,1)
            irec=irec+1
          ELSE
            workg(:,:,1)=-1.0d0 * cov_infl_mul
          ENDIF
        ENDIF

       CALL scatter_ngrd_mpi(0,workg(:,:,1),work2d(:,iv),1)
    ENDDO
    IF( myrank == 0 )CLOSE(55)
  END IF

  !
  ! RTPS relaxation
  !
  IF(RELAX_ALPHA_SPREAD /= 0.0d0) THEN
    ALLOCATE( work3da(nij1,nlev,nv3d) )
    ALLOCATE( work2da(nij1,nv2d) )
    work3da = 1.0d0
    work2da = 1.0d0
  END IF

  !WRITE(*,*)work3d(10,10,2),work2d(10,1)
  !
  ! p_full for background ensemble mean
  !
  ALLOCATE(logpfm(nij1,nlev),zfm(nij1,nlev))
  logpfm = DLOG(mean3dg(:,:,iv3d_p))
  zfm    = mean3dg(:,:,iv3d_ph)/gg  !Compute z in meters.
  !
  ! ESTIMATE 0D PARAMETERS
  !

  !Get number of 0d estimated parameters
  np0d=SUM(update_parameter_0d)
  IF(np0d .GT. 0 .AND. ESTPAR )THEN
    CALL estimate_0d_par
  ENDIF

  !
  ! MAIN ASSIMILATION LOOP
  !
  WRITE(6,'(A,I3)') 'nlev = ',nlev
  ALLOCATE( hdxf(1:nobstotal,1:nbv),rdiag(1:nobstotal),rloc(1:nobstotal),dep(1:nobstotal) )
  DO ilev=1,nlev
    WRITE(6,'(A,I3)') 'ilev = ',ilev
 

!$OMP PARALLEL DO SCHEDULE(DYNAMIC) PRIVATE(ij,n,hdxf,rdiag,rloc,dep,nobsl,parm,trans_p,trans,transm,transrlx,pa,m,k,q_mean,q_sprd,q_anal)
    DO ij=1,nij1

      DO n=1,nv3d
        IF(var_local_n2n(n) < n) THEN
          trans(:,:,n) = trans(:,:,var_local_n2n(n))
          transm(:,n) = transm(:,var_local_n2n(n))                                     !GYL
          IF(RELAX_ALPHA_SPREAD /= 0.0d0) THEN                                         !GYL
            pa(:,:,n) = pa(:,:,var_local_n2n(n))                                       !GYL
          END IF                                                                       !GYL
          work3d(ij,ilev,n) = work3d(ij,ilev,var_local_n2n(n))
        ELSE
          CALL obs_local(ij,ilev,n,hdxf,rdiag,rloc,dep,nobsl,logpfm,zfm)
          parm = work3d(ij,ilev,n)
          IF(RELAX_ALPHA_SPREAD /= 0.0d0) THEN                                         !GYL
            CALL letkf_core(nbv,nobstotal,nobsl,hdxf,rdiag,rloc,dep,parm, &            !GYL
                            trans(:,:,n),transm=transm(:,n),pao=pa(:,:,n),minfl=MIN_INFL_MUL) !GYL
          ELSE                                                                         !GYL
            CALL letkf_core(nbv,nobstotal,nobsl,hdxf,rdiag,rloc,dep,parm, &            !GYL
                            trans(:,:,n),transm=transm(:,n),minfl=MIN_INFL_MUL)        !GYL
          END IF                                                                       !GYL
          work3d(ij,ilev,n) = parm
        END IF
        IF((n == iv3d_qv .OR. n == iv3d_qc .OR. n == iv3d_qr .OR. n == iv3d_qci .OR. n == iv3d_qs .OR. n == iv3d_qg) &
           .AND. ilev > LEV_UPDATE_Q) THEN !GYL, do not update upper-level q,qc
          anal3d(ij,ilev,:,n) = mean3dg(ij,ilev,n) + gues3d(ij,ilev,:,n)                !GYL
        ELSE                                                                           !GYL
          IF(RELAX_ALPHA /= 0.0d0) THEN                                                !GYL - RTPP method (Zhang et al. 2005)
            CALL weight_RTPP(trans(:,:,n),transrlx)                                    !GYL
          ELSE IF(RELAX_ALPHA_SPREAD /= 0.0d0) THEN                                    !GYL - RTPS method (Whitaker and Hamill 2012)
            CALL weight_RTPS(trans(:,:,n),pa(:,:,n),gues3d(ij,ilev,:,n),transrlx,work3da(ij,ilev,n)) !GYL
          ELSE                                                                         !GYL
            transrlx = trans(:,:,n)                                                    !GYL
          END IF                                                                       !GYL
          DO m=1,nbv
            anal3d(ij,ilev,m,n) = mean3dg(ij,ilev,n)
            DO k=1,nbv
              anal3d(ij,ilev,m,n) = anal3d(ij,ilev,m,n) &                              !GYL - sum trans and transm here
                & + gues3d(ij,ilev,k,n) * (transrlx(k,m) + transm(k,n))                !GYL
            END DO
          END DO
          END IF                                                                         !GYL
      END DO ! [ n=1,nv3d ]


      IF(ilev == 1) THEN !update 2d variable at ilev=1
        DO n=1,nv2d
          IF(var_local_n2n(nv3d+n) < nv3d+n) THEN
            trans(:,:,nv3d+n) = trans(:,:,var_local_n2n(nv3d+n))
            transm(:,nv3d+n) = transm(:,var_local_n2n(nv3d+n))                         !GYL
            IF(RELAX_ALPHA_SPREAD /= 0.0d0) THEN                                       !GYL
              pa(:,:,nv3d+n) = pa(:,:,var_local_n2n(nv3d+n))                           !GYL
            END IF                                                                     !GYL
            IF(var_local_n2n(nv3d+n) <= nv3d) THEN                                     !GYL - correct the bug of the 2d variable update
              work2d(ij,n) = work3d(ij,ilev,var_local_n2n(nv3d+n))                     !GYL
            ELSE                                                                       !GYL
              work2d(ij,n) = work2d(ij,var_local_n2n(nv3d+n)-nv3d)                     !GYL
            END IF                                                                     !GYL
          ELSE
            CALL obs_local(ij,ilev,n,hdxf,rdiag,rloc,dep,nobsl,logpfm,zfm)
            parm = work2d(ij,n)
            IF(RELAX_ALPHA_SPREAD /= 0.0d0) THEN                                       !GYL
              CALL letkf_core(nbv,nobstotal,nobsl,hdxf,rdiag,rloc,dep,parm, &       !GYL
                              trans(:,:,nv3d+n),transm=transm(:,nv3d+n),pao=pa(:,:,nv3d+n),minfl=MIN_INFL_MUL) !GYL
            ELSE                                                                       !GYL
              CALL letkf_core(nbv,nobstotal,nobsl,hdxf,rdiag,rloc,dep,parm, &          !GYL
                              trans(:,:,nv3d+n),transm=transm(:,nv3d+n),minfl=MIN_INFL_MUL) !GYL
            END IF                                                                     !GYL
            work2d(ij,n) = parm
          END IF
          IF(RELAX_ALPHA /= 0.0d0) THEN                                                !GYL - RTPP method (Zhang et al. 2005)
            CALL weight_RTPP(trans(:,:,nv3d+n),transrlx)                               !GYL
          ELSE IF(RELAX_ALPHA_SPREAD /= 0.0d0) THEN                                    !GYL - RTPS method (Whitaker and Hamill 2012)
            CALL weight_RTPS(trans(:,:,nv3d+n),pa(:,:,nv3d+n),gues2d(ij,:,n),transrlx,work2da(ij,n)) !GYL
          ELSE                                                                         !GYL
            transrlx = trans(:,:,nv3d+n)                                               !GYL
          END IF                                                                       !GYL
          DO m=1,nbv
            anal2d(ij,m,n) = mean2dg(ij,n)
            DO k=1,nbv
              anal2d(ij,m,n) = anal2d(ij,m,n) &                                        !GYL - sum trans and transm here
                & + gues2d(ij,k,n) * (transrlx(k,m) + transm(k,nv3d+n))                !GYL
            END DO
          END DO
        END DO ! [ n=1,nv2d ]
      END IF ! [ ilev == 1 ]


      IF(ilev == 1 .AND. ESTPAR) THEN !update 2d parameters.
        DO n=1,np2d
          !Check that we have to estimate this parameter and if the parameter is not undef.
          IF( update_parameter_2d(n) == 1 .AND. guesp2d(ij,1,n) .NE. REAL(REAL(UNDEF,r_sngl),r_size) )THEN
          !Weigths will be recomputed for the first parameter.

           IF(TRANSPAR)THEN !Transform parameter if necessary.
            CALL PARAM_TRANSFORM(guesp2d(ij,:,n),param_max_value(n),param_min_value(n),1)
           ENDIF

           CALL com_mean(nbv,guesp2d(ij,:,n),meanp2d(ij,n))  !Compute ensemble mean.
           DO m=1,nbv
            guesp2d(ij,m,n) = guesp2d(ij,m,n) - meanp2d(ij,n) !Compute ensemble perturbations.
           ENDDO


           IF(var_local_par_n2n(n) < n ) THEN
            trans_p(:,:,n) = trans_p(:,:,var_local_par_n2n(n))
           ELSE
            CALL obs_local_p2d(parameter_localization_type_2d(n),ij,ilev,n,hdxf,rdiag,rloc,dep,nobsl,logpfm,zfm)
            CALL letkf_core(nbv,nobstotal,nobsl,hdxf,rdiag,rloc,dep,parm,trans_p(:,:,n))
           END IF
           DO m=1,nbv
            analp2d(ij,m,n)  = meanp2d(ij,n)
            DO k=1,nbv
              analp2d(ij,m,n) = analp2d(ij,m,n) + guesp2d(ij,k,n) * trans_p(k,m,n)
            END DO
           END DO
          
           !Reconstruct parameter gues.
           DO m=1,nbv
            guesp2d(ij,m,n) = meanp2d(ij,n) + guesp2d(ij,m,n)
           END DO

           !Apply multiplicative inflation
           IF( SUM(guesp2d(ij,:,n)) .GT. 0)THEN
            CALL PARAMETER_INFLATION(analp2d(ij,:,n),guesp2d(ij,:,n),trans_p(:,:,n),parameter_fixinflation(n), &
                                     param_sprd_init(n),parameter_inflation_type)
           ENDIF
           !Apply additive inflation
           IF(ADDINFPAR)THEN
             CALL PARAMETER_INFLATION_ADDITIVE(analp2d(ij,:,n),additive_inflation_factor)
           ENDIF
           IF(TRANSPAR)THEN
            CALL PARAM_TRANSFORM(analp2d(ij,:,n),param_max_value(n),param_min_value(n),2)   !Anti transform parameter analysis.
            CALL PARAM_TRANSFORM(guesp2d(ij,:,n),param_max_value(n),param_min_value(n),2)   !Anti transform parameter gues.
           ELSE 
            !If the ensemble mean is outside the meaningful range bring it back.
            CALL com_mean(nbv,analp2d(ij,:,n),meanp2d(ij,n))  !Compute ensemble mean.
            IF(meanp2d(ij,n) > param_max_value(n))THEN
             analp2d(ij,:,n)=analp2d(ij,:,n)+(param_max_value(n)-meanp2d(ij,n))
             CALL com_mean(nbv,analp2d(ij,:,n),meanp2d(ij,n))  !Re compute ensemble mean.
            ENDIF
            IF(meanp2d(ij,n) < param_min_value(n))THEN
             analp2d(ij,:,n)=analp2d(ij,:,n)+(param_min_value(n)-meanp2d(ij,n))
             CALL com_mean(nbv,analp2d(ij,:,n),meanp2d(ij,n))  !Re compute ensemble mean.
            ENDIF
           ENDIF
          ENDIF
       ENDDO  ! [ n=1,np2d ]
      ENDIF  ! [ ilev == 1 , estpar=.true. ]

     END DO  !En do over ij
!$OMP END PARALLEL DO
  ENDDO !En do over ilev


  !CONDENSATES AND QVAPOR CANNOT BE 0 AFTER ASSIMILATION.
  WHERE( anal3d(:,:,:,iv3d_qr) <= 0.0d0)anal3d(:,:,:,iv3d_qr)=0.0d0
  WHERE( anal3d(:,:,:,iv3d_qg) <= 0.0d0)anal3d(:,:,:,iv3d_qg)=0.0d0
  WHERE( anal3d(:,:,:,iv3d_qs) <= 0.0d0)anal3d(:,:,:,iv3d_qs)=0.0d0
  WHERE( anal3d(:,:,:,iv3d_qc) <= 0.0d0)anal3d(:,:,:,iv3d_qc)=0.0d0
  WHERE( anal3d(:,:,:,iv3d_qci) <= 0.0d0)anal3d(:,:,:,iv3d_qci)=0.0d0
  WHERE( anal3d(:,:,:,iv3d_qv) <= 0.0d0)anal3d(:,:,:,iv3d_qv)=0.0d0

  DEALLOCATE(hdxf,rdiag,rloc,dep)
  !
  ! Compute analyses of observations (Y^a)
  ! comentado 16/12 se calcula en monit_obs_ab
  !IF(obsanal_output) THEN                       !G, como obsanal_output es
  !TRUE.
  !  call das_letkf_obs2(work3dg,work2dg,logpfm,zfm)         !G  hace esto y
  !  escribe con write_obs2
  !END IF                                        !G

  IF(RELAX_ALPHA_SPREAD /= 0.0d0) THEN
    DEALLOCATE(work3da,work2da)
  END IF

  !!!!!
  !SMOOTH 2D PARAMETER UPDATE TO AVOID NOISE ACCUMULATION IN SMALL SCALES.
  !!!!!
  IF(ESTPAR)THEN
  IF(smooth_par_update_flag)THEN
  WRITE(6,*)"BEGIN PARAMETER SMOOTHING"
  !Smoothing can affect the mean or the spread of the ensemble. 
  !After smoothing the spread should be restored to their previous value.
  CALL smooth_par_update(analp2d,guesp2d,nbv,update_parameter_2d)

  WRITE(6,*)"END PARAMETER SMOOTHING"
  ENDIF 
  ENDIF

  !WRITE MULTIPLICATIVE INFLATION
  IF(cov_infl_mul < 0.0d0) THEN
    IF(myrank == 0)THEN
    INQUIRE(IOLENGTH=iolen) iolen
    OPEN(55,FILE=inflfile,FORM='unformatted',ACCESS='direct',RECL=nlon*nlat*iolen)
    !OPEN(55,FILE=inflfile,FORM='unformatted',ACCESS='sequential')
    ENDIF
    irec=1
    DO iv=1,nv3d
     !DO ilev=1,nlev
       CALL gather_ngrd_mpi(0,work3d(:,:,iv),workg,nlev)
       IF(myrank == 0) THEN
        DO ilev=1,nlev
         WRITE(55,rec=irec)workg(:,:,ilev)
         irec=irec+1
        ENDDO
       ENDIF
    ENDDO

    DO iv=1,nv2d
       CALL gather_ngrd_mpi(0,work2d(:,iv),workg(:,:,1),1)
       IF(myrank == 0) THEN
        WRITE(55,rec=irec)workg(:,:,1)
        irec=irec+1
       END IF
    ENDDO

    IF(myrank==0)CLOSE(55)
    DEALLOCATE(workg,work3d,work2d)
  END IF


  DEALLOCATE(logpfm,zfm,mean3da,mean2da,mean3dg,mean2dg)
  IF(ESTPAR)DEALLOCATE(meanp2d)
  RETURN
END SUBROUTINE das_letkf

!-----------------------------------------------------------------------
! Trnasform parameters to keep them within their physical meaningful range.
!-----------------------------------------------------------------------
SUBROUTINE PARAM_TRANSFORM(par_ens,pmax,pmin,ts)
REAL(r_size),INTENT(INOUT) ::   par_ens(nbv)
REAL(r_size),INTENT(IN)    ::   pmax,pmin
INTEGER      ::   i
REAL(r_size) ::   a,b,x
INTEGER      ::   ts  !If ts=1 transform, if ts=2 anti transform

 a=0.5d0*(pmax-pmin)
 b=0.5d0*(pmax+pmin)

 IF(ts==1)THEN
   DO i=1,nbv
    x=(par_ens(i)-b)/a
    IF(abs(x) >= 1)THEN
      WRITE(*,*)"WARNING, INVERSE HYPERBOLIC TANGENT CANNOT BE COMPUTED"
      WRITE(*,*)"PARAMETER= ",par_ens(i)," X = ",x," a = ",a, " b= ", b
      !This usually happens for boundary points.
      CYCLE
    ENDIF
      par_ens(i)=0.5*log( (x+1)/(1-x) )
   ENDDO
 ELSEIF(ts==2)THEN
   DO i=1,nbv
    x=par_ens(i)
    x=( exp(2*x) - 1 )/( exp(2*x) + 1)
    par_ens(i)=a*x+b
   ENDDO
 ELSE

 ENDIF

END SUBROUTINE PARAM_TRANSFORM

!------------------------------------------------------------------------
! PARAMETER_INFLATION: Apply different types of inflation to the parameter
! ensemble.
!------------------------------------------------------------------------

SUBROUTINE PARAMETER_INFLATION(anal_par_ens,gues_par_ens,w,fixedlambda,fix_sprd,pinf_type)

IMPLICIT NONE
INTEGER            :: n,m,k
REAL(r_size), INTENT(INOUT)       :: anal_par_ens(nbv)
REAL(r_size), INTENT(IN)          :: gues_par_ens(nbv)
REAL(r_size), INTENT(IN)          :: fixedlambda
REAL(r_size), INTENT(IN)          :: fix_sprd
INTEGER, INTENT(IN)               :: pinf_type
REAL(r_size)                      :: tmp_siga,tmp_sigg,tmp_meana,tmp_meang,lambda,fixstd
REAL(r_size), INTENT(IN)          :: w(nbv,nbv)
REAL(r_size)                      :: w_par(nbv,nbv)
REAL(r_size)                      :: w_var(nbv),w_mean(nbv)


IF(pinf_type==1)THEN
!PARAMETER INFLATION TYPE: AKSOY FIXED PARAMETER SPREAD.    
      CALL com_mean(nbv,anal_par_ens,tmp_meana)
      !CALL com_stdev(nbv,gues_par_ens,tmp_sigg)
      CALL com_stdev(nbv,anal_par_ens,tmp_siga) 
      !Only apply this inflation is the background spread is greather than 0.     
      IF(tmp_siga .GT. 0)THEN
      lambda=fix_sprd/tmp_siga
      DO m=1,nbv
        anal_par_ens(m)=(anal_par_ens(m)-tmp_meana)*lambda+tmp_meana
      END DO
      ENDIF

!FIXED PARAMETER INFLATION AS IN KW2010 
ELSEIF(pinf_type==2 )THEN
     CALL com_mean(nbv,anal_par_ens,tmp_meana)
     DO m=1,nbv
        anal_par_ens(m)=(anal_par_ens(m)-tmp_meana)*(fixedlambda)+tmp_meana
     END DO

ELSEIF(pinf_type==3)THEN

!W INFLATION TECHNIQUE FOR THE PARAMETER
! Inflate variance of W columns so the equal the varians of the 
! W corresponding to the background (i.e. the identity)
      w_par=w    !To preserve the original w value.
      w_mean=0.0d0
      DO m=1,nbv
         CALL com_mean(nbv,w_par(m,:),w_mean(m))
       DO k=1,nbv
            w_par(m,k)=w_par(m,k)-w_mean(m)
       ENDDO
      ENDDO

      !Compute W column variance
      w_var=0.0d0
      DO m=1,nbv
          CALL com_covar(nbv,w_par(:,m),w_par(:,m),w_var(m))
      ENDDO


      !Defino un coeficiente de inflacion tal que la std de las columnas de W sean iguales a las que tenia originalmente.   
      lambda=SQRT( 1 / SUM(w_var) )
      CALL com_mean(nbv,anal_par_ens,tmp_meana)
      DO m=1,nbv
        anal_par_ens(m)=(anal_par_ens(m)-tmp_meana)*lambda+tmp_meana
      ENDDO
ELSE
     WRITE(*,*) "ERROR: UNRECOGNIZED PARAMETER INFLATION TYPE!!: ",pinf_type
     STOP 99
ENDIF

RETURN
END SUBROUTINE PARAMETER_INFLATION

!------------------------------------------------------------------------
! PARAMETER ADDITIVE INFLATION
!------------------------------------------------------------------------

SUBROUTINE PARAMETER_INFLATION_ADDITIVE(anal_par_ens,additive_inflation_factor)

IMPLICIT NONE
INTEGER            :: n,m,k
REAL(r_size), INTENT(INOUT)       :: anal_par_ens(nbv)
REAL(r_size), INTENT(IN)          :: additive_inflation_factor
REAL(r_size)                      :: tmp_meana,tmp_siga
REAL(r_size)                      :: random_noise(nbv)

      CALL com_stdev(nbv,anal_par_ens,tmp_siga)  
      CALL com_randn(nbv,random_noise)     
      !APPLY ADDITIVE INFLATION.
        
      DO m=1,nbv
        anal_par_ens(m)=anal_par_ens(m)+random_noise(m)*additive_inflation_factor*tmp_siga
      END DO

END SUBROUTINE PARAMETER_INFLATION_ADDITIVE

!-----------------------------------------------------------------------
! Project global observations to local
!     (hdxf_g,dep_g,rdiag_g) -> (hdxf,dep,rdiag)
!-----------------------------------------------------------------------
SUBROUTINE obs_local(ij,ilev,nvar,hdxf,rdiag,rloc,dep,nobsl,logpfm,zfm,oindex)
  IMPLICIT NONE
  INTEGER,INTENT(IN) :: ij,ilev,nvar
  REAL(r_size),INTENT(IN) :: logpfm(nij1,nlev),zfm(nij1,nlev)
  REAL(r_size),INTENT(OUT) :: hdxf(nobstotal,nbv)
  REAL(r_size),INTENT(OUT) :: rdiag(nobstotal)
  REAL(r_size),INTENT(OUT) :: rloc(nobstotal)
  REAL(r_size),INTENT(OUT) :: dep(nobstotal)
  INTEGER,INTENT(OUT),OPTIONAL :: oindex(nobstotal)      ! DH
  REAL(r_size)  ::  sigmah , sigmav
  INTEGER,INTENT(OUT) :: nobsl
  REAL(r_size) :: dist,dlev
  INTEGER,ALLOCATABLE:: nobs_use(:)
  INTEGER :: imin,imax,jmin,jmax,im,ichan,ielm
  INTEGER :: n,nn,iobs
!
! INITIALIZE
!
  
  IF( nobs > 0 ) THEN
    ALLOCATE(nobs_use(nobs))
  END IF
!
! data search
!
  imin = MAX(NINT(ri1(ij) - dist_zeroij),1)
  imax = MIN(NINT(ri1(ij) + dist_zeroij),nlon)
  jmin = MAX(NINT(rj1(ij) - dist_zeroij),1)
  jmax = MIN(NINT(rj1(ij) + dist_zeroij),nlat)

  nn = 1
  IF( nobs > 0 ) CALL obs_local_sub(imin,imax,jmin,jmax,nn,nobs_use)
  nn = nn-1
  IF(nn < 1) THEN
    nobsl = 0
    RETURN
  END IF
!
! CONVENTIONAL
!
  nobsl = 0
  IF(nn > 0) THEN
    DO n=1,nn
      !
      ! vertical localization
      !
      ielm=NINT(obselm(nobs_use(n)))
      IF( ielm >= id_tclon_obs) THEN !TC track obs
        dlev = 0.0d0
      !ELSE IF( ielm == id_ps_obs .AND. ilev > 1) THEN
      ELSE IF( ielm == id_ps_obs ) THEN
        dlev = ABS(LOG(obsdat(nobs_use(n))) - logpfm(ij,ilev)) ! diferencia de presion entre nivel observado y el modelo
        IF(dlev > dist_zerov) CYCLE
      ELSE IF( ielm == id_u_obs .OR. ielm == id_v_obs .OR. ielm == id_t_obs .OR. ielm == id_q_obs & 
      .OR. ielm == id_tv_obs .OR. ielm == id_rh_obs .OR. ielm==id_co_obs .OR. ielm==id_totco_obs ) THEN
        dlev = ABS(LOG(obslev(nobs_use(n))) - logpfm(ij,ilev))
        IF(dlev > dist_zerov) CYCLE
      ELSE IF( ielm == id_reflectivity_obs .OR.   &
        ielm == id_radialwind_obs .OR. ielm == id_pseudorh_obs ) THEN
        dlev = ABS( obslev(nobs_use(n)) - zfm(ij,ilev) ) !Vertical localization in Z.
        IF(dlev > dist_zeroz) CYCLE
      ELSE IF( ( ielm == id_ts_obs .or. ielm == id_qs_obs .or. ielm == id_rhs_obs .or. &
               ielm == id_us_obs .or. ielm == id_vs_obs ) )THEN
!        dlev= ABS( zfm(ij,1) - zfm(ij,ilev) ) !Vertical localization in Z assuming observation is at the lowest model level.
!        if( dlev > dist_zeroz ) CYCLE
        dlev= ABS(LOG(1.0e5 * exp( - obslev(nobs_use(n)) *9.81/287.05/290.0 ))- logpfm(ij,ilev)) ! Vertical localization in P using the observation height
        if( dlev > dist_zerov ) CYCLE   
      ELSE
        dlev = 0.0d0
      END IF

      SELECT CASE(NINT(obselm(nobs_use(n))))
      CASE(id_u_obs,id_v_obs,id_t_obs,id_tv_obs,id_q_obs,id_rh_obs)
        sigmah=sigma_obs
        sigmav=sigma_obsv
      CASE(id_ps_obs)
        sigmah=sigma_obs
        sigmav=sigma_obsv
      CASE(id_reflectivity_obs,id_radialwind_obs,id_pseudorh_obs)
        sigmah=sigma_obs_radar
        sigmav=sigma_obsz
      CASE(id_ts_obs,id_qs_obs,id_rhs_obs,     &
           id_us_obs,id_vs_obs )
        sigmah=sigma_obs
        sigmav=sigma_obsz     
      END SELECT

      

      !
      ! horizontal localization
      !
      CALL com_distll_1(obslon(nobs_use(n)),obslat(nobs_use(n)),&
        & lon1(ij),lat1(ij),dist)
      IF( ielm == id_reflectivity_obs .OR.   &
          ielm == id_radialwind_obs .OR. ielm == id_pseudorh_obs ) THEN
                IF(dist > dist_zero_radar) CYCLE
      ELSE 
                IF(dist > dist_zero) CYCLE
      END IF 
      !
      ! variable localization
      !
      CALL get_iobs( NINT(obselm(nobs_use(n))) , iobs)

      IF(var_local(nvar,iobs) < TINY(var_local)) CYCLE

      nobsl = nobsl + 1
      hdxf(nobsl,:) = obshdxf(nobs_use(n),:)
      dep(nobsl)    = obsdep(nobs_use(n))
      !
      ! Observational localization
      !

      rdiag(nobsl) = obserr(nobs_use(n))**2
      rloc(nobsl) =EXP(-0.5d0 * ((dist/sigmah)**2 + (dlev/sigmav)**2)) &
                  & * var_local(nvar,iobs)

      IF(PRESENT(oindex)) oindex(nobsl) = nobs_use(n)      ! DH
    END DO
  END IF

  IF( nobsl > nobstotal ) THEN
    WRITE(6,'(A,I5,A,I5)') 'FATAL ERROR, NOBSL=',nobsl,' > NOBSTOTAL=',nobstotal
    WRITE(6,*) 'IJ,NN=', ij, nn
    STOP 99
  END IF
!
  IF( nobs > 0 ) THEN
    DEALLOCATE(nobs_use)
  END IF
!
  RETURN
END SUBROUTINE obs_local

!-----------------------------------------------------------------------
! SUBROUTINE obs_local2  (adaptacion SMN/git)
! es utilizada para que no use la localizacion 
! del tipo de variable en el obs_local
! el nvar = 0
!-----------------------------------------------------------------------
SUBROUTINE obs_local2(ij,ilev,nvar,hdxf,rdiag,rloc,dep,nobsl,logpfm,zfm,oindex)
  IMPLICIT NONE
  INTEGER,INTENT(IN) :: ij,ilev,nvar
  REAL(r_size),INTENT(IN) :: logpfm(nij1,nlev),zfm(nij1,nlev)
  REAL(r_size),INTENT(OUT) :: hdxf(nobstotal,nbv)
  REAL(r_size),INTENT(OUT) :: rdiag(nobstotal)
  REAL(r_size),INTENT(OUT) :: rloc(nobstotal)
  REAL(r_size),INTENT(OUT) :: dep(nobstotal)
  INTEGER,INTENT(OUT),OPTIONAL :: oindex(nobstotal)      ! DH
  REAL(r_size)  ::  sigmah , sigmav
  INTEGER,INTENT(OUT) :: nobsl
  REAL(r_size) :: dist,dlev
  INTEGER,ALLOCATABLE:: nobs_use(:)
  INTEGER :: imin,imax,jmin,jmax,im,ichan,ielm
  INTEGER :: n,nn,iobs
!
! INITIALIZE
!
 
  IF( nobs_efso > 0 ) THEN
    ALLOCATE(nobs_use(nobs_efso))
  END IF
!
! data search
!
  imin = MAX(NINT(ri1(ij) - dist_zeroij),1)
  imax = MIN(NINT(ri1(ij) + dist_zeroij),nlon)
  jmin = MAX(NINT(rj1(ij) - dist_zeroij),1)
  jmax = MIN(NINT(rj1(ij) + dist_zeroij),nlat)

  nn = 1
  IF( nobs_efso > 0 ) CALL obs_local_sub2(imin,imax,jmin,jmax,nn,nobs_use)
  nn = nn-1
  IF(nn < 1) THEN
    nobsl = 0
    RETURN
  END IF
!
! CONVENTIONAL
!
  nobsl = 0
  IF(nn > 0) THEN
    DO n=1,nn
      !
      ! vertical localization
      !
      ielm=NINT(obselm(nobs_use(n)))
      IF( ielm >= id_tclon_obs) THEN !TC track obs
        dlev = 0.0d0
      !ELSE IF( ielm == id_ps_obs .AND. ilev > 1) THEN
      ELSE IF( ielm == id_ps_obs ) THEN
        dlev = ABS(LOG(obsdat(nobs_use(n))) - logpfm(ij,ilev)) ! diferencia entre nivel observado y el modelo
        IF(dlev > dist_zerov) CYCLE
      ELSE IF( ielm == id_u_obs .OR. ielm == id_v_obs .OR. ielm == id_t_obs .OR. ielm == id_q_obs &
      .OR. ielm == id_tv_obs .OR. ielm == id_rh_obs .OR. ielm==id_co_obs .OR. ielm==id_totco_obs ) THEN
        dlev = ABS(LOG(obslev(nobs_use(n))) - logpfm(ij,ilev))
        IF(dlev > dist_zerov) CYCLE
      ELSE IF( ielm == id_reflectivity_obs .OR.   &
        ielm == id_radialwind_obs .OR. ielm == id_pseudorh_obs ) THEN
        dlev = ABS( obslev(nobs_use(n)) - zfm(ij,ilev) ) !Vertical localization in Z.
        IF(dlev > dist_zeroz) CYCLE
      ELSE IF( ( ielm == id_ts_obs .or. ielm == id_qs_obs .or. ielm == id_rhs_obs .or. &
               ielm == id_us_obs .or. ielm == id_vs_obs ) )THEN
!        dlev= ABS( zfm(ij,1) - zfm(ij,ilev) ) !Vertical localization in Z assuming observation is at the lowest model level.
!        if( dlev > dist_zeroz ) CYCLE
        dlev= ABS(LOG(1.0e5 * exp( - obslev(nobs_use(n)) *9.81/287.05/290.0 ))-logpfm(ij,ilev)) ! Vertical localization in P using the observation height
        if( dlev > dist_zerov ) CYCLE

      ELSE
        dlev = 0.0d0
      END IF

      SELECT CASE(NINT(obselm(nobs_use(n))))
      CASE(id_u_obs,id_v_obs,id_t_obs,id_tv_obs,id_q_obs,id_rh_obs)
        sigmah=sigma_obs
        sigmav=sigma_obsv
      CASE(id_ps_obs)
        sigmah=sigma_obs
        sigmav=sigma_obsv
      CASE(id_reflectivity_obs,id_radialwind_obs,id_pseudorh_obs)
        !WRITE(6,*) 'RADAR CASE'
        sigmah=sigma_obs_radar
        sigmav=sigma_obsz
      CASE(id_ts_obs,id_qs_obs,id_rhs_obs,     &
           id_us_obs,id_vs_obs )
        sigmah=sigma_obs
        sigmav=sigma_obsz
      END SELECT
      !
      ! horizontal localization
      !
      CALL com_distll_1(obslon(nobs_use(n)),obslat(nobs_use(n)),&
        & lon1(ij),lat1(ij),dist)
      IF( ielm == id_reflectivity_obs .OR.   &
          ielm == id_radialwind_obs .OR. ielm == id_pseudorh_obs ) THEN
                IF(dist > dist_zero_radar) CYCLE
      ELSE
                IF(dist > dist_zero) CYCLE
      END IF
      !
      ! variable localization
      !
      IF(nvar > 0) THEN ! use variable localization only when nvar > 0
        CALL get_iobs( NINT(obselm(nobs_use(n))) , iobs)

        IF(var_local(nvar,iobs) < TINY(var_local)) CYCLE
      END IF

      nobsl = nobsl + 1
      hdxf(nobsl,:) = obshdxf(nobs_use(n),:)
      dep(nobsl)    = obsdep(nobs_use(n))
      !
      ! Observational localization
      !

      rdiag(nobsl) = obserr(nobs_use(n))**2
      rloc(nobsl) =EXP(-0.5d0 * ((dist/sigmah)**2 + (dlev/sigmav)**2))
      IF(nvar > 0) THEN
        rloc(nobsl) = rloc(nobsl) * var_local(nvar,iobs)
      END IF

      IF(PRESENT(oindex)) oindex(nobsl) = nobs_use(n)      ! DH
    END DO
  END IF
  !WRITE(6,*)'NOBSL = ', nobsl
  IF( nobsl > nobstotal ) THEN
    WRITE(6,'(A,I5,A,I5)') 'FATAL ERROR, NOBSL=',nobsl,' > NOBSTOTAL=',nobstotal
    WRITE(6,*) 'IJ,NN=', ij, nn
    STOP 99
  END IF
!
  IF( nobs_efso > 0 ) THEN
    DEALLOCATE(nobs_use)
  END IF
!
  RETURN
END SUBROUTINE obs_local2


!-----------------------------------------------------------------------
! Project global observations to local
!     (hdxf_g,dep_g,rdiag_g) -> (hdxf,dep,rdiag)
!-----------------------------------------------------------------------
SUBROUTINE obs_local_p2d(loctype,ij,ilev,nvar,hdxf,rdiag,rloc,dep,nobsl,logpfm,zfm)
  IMPLICIT NONE
  INTEGER,INTENT(IN) :: ij,ilev,nvar,loctype
  REAL(r_size),INTENT(IN) :: logpfm(nij1,nlev),zfm(nij1,nlev)
  REAL(r_size),INTENT(OUT) :: hdxf(nobstotal,nbv)
  REAL(r_size),INTENT(OUT) :: rdiag(nobstotal)
  REAL(r_size),INTENT(OUT) :: rloc(nobstotal)
  REAL(r_size),INTENT(OUT) :: dep(nobstotal)
  REAL(r_size)             :: sigmah , sigmav
  INTEGER,INTENT(OUT) :: nobsl
  REAL(r_size) :: dist,dlev
  INTEGER,ALLOCATABLE:: nobs_use(:)
  INTEGER :: imin,imax,jmin,jmax,im,ichan
  INTEGER :: n,nn,iobs,ielm
!
! INITIALIZE
!
  IF( nobs > 0 ) THEN
    ALLOCATE(nobs_use(nobs))
  END IF
!
! data search
!
  imin = MAX(NINT(ri1(ij) - dist_zeroij),1)
  imax = MIN(NINT(ri1(ij) + dist_zeroij),nlon)
  jmin = MAX(NINT(rj1(ij) - dist_zeroij),1)
  jmax = MIN(NINT(rj1(ij) + dist_zeroij),nlat)

  nn = 1
  IF( nobs > 0 ) CALL obs_local_sub(imin,imax,jmin,jmax,nn,nobs_use)
  nn = nn-1
  IF(nn < 1) THEN
    nobsl = 0
    RETURN
  END IF
!
! CONVENTIONAL
!
  
  nobsl = 0
  IF(nn > 0) THEN
    DO n=1,nn
      !
      ! vertical localization
      !
     IF ( loctype == 1 )THEN !Vertical localization will be used.

      !
      ! vertical localization
      !
      ielm=NINT(obselm(nobs_use(n)))
      IF( ielm >= id_tclon_obs) THEN !TC track obs
        dlev = 0.0d0    
      ELSE IF( ielm == id_ps_obs .AND. ilev > 1) THEN
        dlev = ABS(LOG(obsdat(nobs_use(n))) - logpfm(ij,ilev))
        IF(dist > dist_zerov) CYCLE
      ELSE IF( ielm == id_u_obs .OR. ielm == id_v_obs .OR. ielm == id_t_obs .OR. ielm == id_q_obs .OR. & 
      ielm == id_tv_obs .OR. ielm == id_rh_obs .OR. ielm==id_co_obs .OR. ielm==id_totco_obs ) THEN
        dlev = ABS(LOG(obslev(nobs_use(n))) - logpfm(ij,ilev))
        IF(dist > dist_zerov) CYCLE
      ELSE IF( ielm == id_reflectivity_obs  .OR. & 
        ielm == id_radialwind_obs .OR. ielm == id_pseudorh_obs ) THEN
        dlev = ABS( obslev(nobs_use(n)) - zfm(ij,ilev) ) !Vertical localization in Z.
        IF(dist > dist_zeroz) CYCLE
      ELSE
        dlev = 0.0d0
      END IF

     ELSEIF( loctype == 2)THEN !No vertical localization.
       dlev = 0.0d0

     ELSE
       WRITE(6,*)'WARNING: Not recognized parameter localization type.',loctype
       STOP
     ENDIF

      !
      ! horizontal localization
      !
      CALL com_distll_1(obslon(nobs_use(n)),obslat(nobs_use(n)),&
        & lon1(ij),lat1(ij),dist)


      IF(dist > dist_zero) CYCLE

      !
      ! variable localization
      !
      !Localization length can be set independently for each observation type.
      CALL get_iobs( NINT(obselm(nobs_use(n))) , iobs)
    

      IF(var_local_par(nvar,iobs) < TINY(var_local)) CYCLE

      nobsl = nobsl + 1
      hdxf(nobsl,:) = obshdxf(nobs_use(n),:)
      dep(nobsl)    = obsdep(nobs_use(n))      
      !
      ! Observational localization
      !
      sigmah=sigma_obs
      sigmav=sigma_obsv

      rdiag(nobsl) = obserr(nobs_use(n))**2
      rloc(nobsl) =EXP(-0.5d0 * ((dist/sigmah)**2 + (dlev/sigmav)**2)) &
                  & * var_local(nvar,iobs)

    END DO
  END IF
!
! DEBUG
! IF( ILEV == 1 .AND. ILON == 1 ) &
! & WRITE(6,*) 'ILEV,ILON,ILAT,NN,TVNN,NOBSL=',ilev,ij,nn,tvnn,nobsl
!
  IF( nobsl > nobstotal ) THEN
    WRITE(6,'(A,I5,A,I5)') 'FATAL ERROR, NOBSL=',nobsl,' > NOBSTOTAL=',nobstotal
    WRITE(6,*) 'IJ,NN=', ij, nn
    STOP 99
  END IF
!
  IF( nobs > 0 ) THEN
    DEALLOCATE(nobs_use)
  END IF
!
  RETURN
END SUBROUTINE obs_local_p2d
!-----------------------------------------------------------------------
! Project global observations to local
!     (hdxf_g,dep_g,rdiag_g) -> (hdxf,dep,rdiag)
!-----------------------------------------------------------------------
SUBROUTINE obs_local_p0d(loctype,nvar,hdxf,rdiag,rloc,dep,nobsl)
  IMPLICIT NONE
  INTEGER,INTENT(IN) :: nvar,loctype
  !REAL(r_size),INTENT(IN) :: logpfm(nij1,nlev)
  REAL(r_size),INTENT(OUT) :: hdxf(nobstotal,nbv)
  REAL(r_size),INTENT(OUT) :: rdiag(nobstotal)
  REAL(r_size),INTENT(OUT) :: rloc(nobstotal)
  REAL(r_size),INTENT(OUT) :: dep(nobstotal)
  INTEGER,INTENT(OUT) :: nobsl
  REAL(r_size) :: dist,dlev
  INTEGER,ALLOCATABLE:: nobs_use(:)
  INTEGER :: imin,imax,jmin,jmax,im,ichan
  INTEGER :: n,nn,iobs
  INTEGER :: tmpi,tmpj

nobsl = 0
  DO n=1,nobs


IF ( loctype == 1 )   THEN
      !
      ! vertical localization (localize as a surface variable)
      !
      IF(NINT(obselm(n)) >= id_tclon_obs) THEN !TC track obs
        dlev = 0.0d0
      ELSE IF(NINT(obselm(n)) == id_ps_obs ) THEN
        dlev = 0.0d0
      ELSE IF(NINT(obselm(n)) /= id_ps_obs) THEN
        dlev = ABS(LOG(obslev(n)) - LOG(100000.0e0)) 

      ELSE
        dlev = 0.0d0
      END IF
      IF(dlev > dist_zerov) CYCLE


END IF  !End for localization type 1

IF  ( loctype == 2)  THEN
            !Do not apply vertical localization

      dlev = 0.0d0

END IF


      !
      ! horizontal localization
      !
      !Discard land observations
      tmpi=INT(obsi(n))
      tmpj=INT(obsj(n))
      IF ( landmask(tmpi,tmpj) /= 0 )CYCLE
      !
      ! variable localization
      !
      CALL get_iobs( NINT(obselm(n)) , iobs)
    
      IF(var_local_par(nvar,iobs) < TINY(var_local_par)) CYCLE

      nobsl = nobsl + 1
      hdxf(nobsl,:) = obshdxf(n,:)
      dep(nobsl)    = obsdep(n)
      !
      ! Observational localization
      !
      rdiag(nobsl) = obserr(n)**2
      rloc(nobsl) =EXP( (dlev/sigma_obsv)**2) &
                  & * var_local(nvar,iobs)
    END DO
!
!
!
!
  RETURN
END SUBROUTINE obs_local_p0d

SUBROUTINE obs_local_sub(imin,imax,jmin,jmax,nn,nobs_use)
  INTEGER,INTENT(IN) :: imin,imax,jmin,jmax
  INTEGER,INTENT(INOUT) :: nn, nobs_use(nobs)
  INTEGER :: j,n,ib,ie,ip

  DO j=jmin,jmax
    IF(imin > 1) THEN
      ib = nobsgrd(imin-1,j)+1
    ELSE
      IF(j > 1) THEN
        ib = nobsgrd(nlon,j-1)+1
      ELSE
        ib = 1
      END IF
    END IF
    ie = nobsgrd(imax,j)
    n = ie - ib + 1
    IF(n == 0) CYCLE
    DO ip=ib,ie
      IF(nn > nobs) THEN
        WRITE(6,*) 'FATALERROR, NN > NOBS', NN, NOBS
      END IF
      nobs_use(nn) = ip
      nn = nn + 1
    END DO
  END DO

  RETURN

END SUBROUTINE obs_local_sub

SUBROUTINE obs_local_sub2(imin,imax,jmin,jmax,nn,nobs_use)
  INTEGER,INTENT(IN) :: imin,imax,jmin,jmax
  INTEGER,INTENT(INOUT) :: nn, nobs_use(nobs_efso)
  INTEGER :: j,n,ib,ie,ip

  DO j=jmin,jmax
    IF(imin > 1) THEN
      ib = nobsgrd(imin-1,j)+1
    ELSE
      IF(j > 1) THEN
        ib = nobsgrd(nlon,j-1)+1
      ELSE
        ib = 1
      END IF
    END IF
    ie = nobsgrd(imax,j)
    n = ie - ib + 1
    IF(n == 0) CYCLE
    DO ip=ib,ie
      IF(nn > nobs_efso) THEN
        WRITE(6,*) 'FATALERROR, NN > NOBS', NN, NOBS_EFSO
      END IF
      nobs_use(nn) = ip
      nn = nn + 1
    END DO
  END DO

  RETURN

END SUBROUTINE obs_local_sub2

SUBROUTINE estimate_0d_par()
IMPLICIT NONE
INTEGER   n,i,npoints,m,counterp,ierr,nobsl,k
REAL(r_size) , ALLOCATABLE :: tmpmeanp0d(:),tmpsprdp0d(:),tmpguesp0d(:,:),tmpanalp0d(:,:),workp0d(:,:),tmptrans_p(:,:,:)
REAL(r_size),ALLOCATABLE :: hdxf0d(:,:)
REAL(r_size),ALLOCATABLE :: rdiag0d(:)
REAL(r_size),ALLOCATABLE :: rloc0d(:)
REAL(r_size),ALLOCATABLE :: dep0d(:)
REAL(r_size)             :: parm


ALLOCATE(tmpmeanp0d(np0d))
ALLOCATE(tmpsprdp0d(np0d))
ALLOCATE(tmpguesp0d(nbv,np0d))
ALLOCATE(tmpanalp0d(nbv,np0d))
ALLOCATE(workp0d(nbv,np0d))
ALLOCATE(np2dtop0d(np0d))
ALLOCATE(tmptrans_p(nbv,nbv,np0d))

  !First spatially average 2d parameters.
  counterp=0
  tmpguesp0d=0.0d0
  DO n=1,np2d
   IF(update_parameter_0d(n)==1)THEN
     counterp=counterp+1
     npoints=0
     np2dtop0d(counterp)=n  !Correspondance between p0d parameters and total parameters array.
     DO i=1,nij1
      IF(landmask1(i) == 0 .AND. guesp2d(i,1,n) /= REAL(REAL(undef,r_sngl),r_size) )THEN
        DO m=1,nbv
         tmpguesp0d(m,counterp)=tmpguesp0d(m,counterp)+guesp2d(i,m,n)
        ENDDO
       npoints=npoints+1
       ENDIF
     ENDDO
   ENDIF
  ENDDO


  !All the processes will have the same spatially averaged parameters.
  workp0d=0.0d0
  CALL MPI_ALLREDUCE(tmpguesp0d,workp0d,np0d*nbv,MPI_DOUBLE_PRECISION,MPI_SUM,&
          & MPI_COMM_WORLD,ierr)
  CALL MPI_ALLREDUCE(npoints,iworkp0d,1,MPI_INTEGER,MPI_SUM,&
          & MPI_COMM_WORLD,ierr)
  !Get the parameter mean

   !WRITE(6,*)"workp0d,iworkp0d : ",workp0d,iworkp0d
   tmpguesp0d=workp0d/iworkp0d


IF(myrank == 0 )THEN
   WRITE(6,*)"UPDATING 0D PARAMETERS"

   !Transform 0d parameters if requested
   IF(TRANSPAR)THEN
    WRITE(6,*)'Transforming parameters'
     DO n=1,np0d
     !Transform parameters using tanh function.
       CALL PARAM_TRANSFORM(tmpguesp0d(:,n),param_max_value(np2dtop0d(n)),param_min_value(np2dtop0d(n)),1)
     ENDDO
   ENDIF




  !Compute mean and perturbations.
    tmpmeanp0d=0.0d0
    DO n=1,np0d
      DO m=1,nbv
         tmpmeanp0d(n)=tmpmeanp0d(n)+tmpguesp0d(m,n)
     ENDDO
       tmpmeanp0d(n)=tmpmeanp0d(n)/REAL(nbv,r_size)
      DO m=1,nbv
       tmpguesp0d(m,n)=tmpguesp0d(m,n)-tmpmeanp0d(n)
      ENDDO
    ENDDO
   ALLOCATE(hdxf0d(nobstotal,nbv),rdiag0d(nobstotal),rloc0d(nobstotal),dep0d(nobstotal))
   DO n=1,np0d
    WRITE(6,*)"PARAMETER ENSEMBLE MEAN (GUES): ",tmpmeanp0d(n)
    WRITE(6,*)"PARAMETER ENSEMBLE (GUES):",tmpguesp0d(:,n)+tmpmeanp0d(n)
   ENDDO
      !Main estimation loop over 0d parameters.
      DO n=1,np0d
          !Observation localization for 0d parameter estimation.
          hdxf0d=0.0d0
          rdiag0d=0.0d0
          rloc0d=0.0d0
          dep0d=0.0d0
          CALL obs_local_p0d(parameter_localization_type_0d(np2dtop0d(n)),np2dtop0d(n),hdxf0d,rdiag0d,rloc0d,dep0d,nobsl)

          WRITE(6,*)nobsl,' observations are used to estimate 0d parameters'
          parm = 1.0d0
          CALL letkf_core(nbv,nobstotal,nobsl,hdxf0d,rdiag0d,rloc0d,dep0d,parm,tmptrans_p(:,:,n))


           !Compute 0d parameter analysis
           DO m=1,nbv
             tmpanalp0d(m,n)  = tmpmeanp0d(n)
              DO k=1,nbv
               tmpanalp0d(m,n) = tmpanalp0d(m,n) + tmpguesp0d(k,n) * tmptrans_p(k,m,n)
              END DO
            END DO
           !Reconstruct parameter gues.


            DO m=1,nbv
              tmpguesp0d(m,n) = tmpmeanp0d(n) + tmpguesp0d(m,n)
            END DO
            tmpmeanp0d=0.0d0
            CALL com_mean(nbv,tmpanalp0d(:,n),tmpmeanp0d(n))
           !Apply multiplicative inflation to the parameter.
           WRITE(6,*)"PARAMETER ENSEMBLE MEAN BEFORE INFLATION (ANAL): ",tmpmeanp0d(n)
           WRITE(6,*)"PARAMETER ENSEMBLE BEFORE INFLATION (ANAL): ",tmpanalp0d(:,n)
           WRITE(6,*)"PARAMETER FIX SPREAD",param_sprd_init(np2dtop0d(n))
           CALL PARAMETER_INFLATION(tmpanalp0d(:,n),tmpguesp0d(:,n),tmptrans_p(:,:,n),  &
           & parameter_fixinflation(np2dtop0d(n)),param_sprd_init(np2dtop0d(n)),parameter_inflation_type)
!
           !Apply additive inflation to the parameter.
           IF(ADDINFPAR)THEN
             CALL PARAMETER_INFLATION_ADDITIVE(tmpanalp0d(:,n),additive_inflation_factor)
           ENDIF
      ENDDO

    tmpmeanp0d=0.0d0
    DO n=1,np0d
      DO m=1,nbv
         tmpmeanp0d(n)=tmpmeanp0d(n)+tmpanalp0d(m,n)
      ENDDO
      tmpmeanp0d(n)=tmpmeanp0d(n)/REAL(nbv,r_size)
    ENDDO

  DEALLOCATE( hdxf0d,rdiag0d,rloc0d,dep0d )

    DO n=1,np0d
    WRITE(6,*)"PARAMETER ENSEMBLE MEAN (ANAL): ",tmpmeanp0d(n)
    WRITE(6,*)"PARAMETER ENSEMBLE (ANAL): ",tmpanalp0d(:,n)
    ENDDO

ENDIF  !END FOR THE IF MY_RANK == 0


  !broadcast the estimated parameters to all process.
  CALL MPI_BCAST( tmpanalp0d, nbv*np0d, MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,ierr)

  !Convert 0d parameters to 2d parameters (only parameter value over the sea
  !will be modified.
  !over the land the default parameter value is assumed. 
  counterp=0
  DO n=1,np2d
   IF(update_parameter_0d(n)==1)THEN
     counterp=counterp+1
     DO i=1,nij1
      IF(landmask1(i) == 0)THEN
        DO m=1,nbv
         analp2d(i,m,n)=tmpanalp0d(m,counterp)
        ENDDO
       ELSE
       !This is to fix the parameter value over land for 0d estimated
       !parameters.
        analp2d(i,:,n)=param_default_value(n)
       ENDIF
     ENDDO
   ENDIF
  ENDDO


RETURN
END SUBROUTINE estimate_0d_par



!-----------------------------------------------------------------------
! Relaxation via LETKF weight - RTPP method
!-----------------------------------------------------------------------
subroutine weight_RTPP(w, wrlx)
  implicit none
  real(r_size), intent(in) :: w(nbv,nbv)
  real(r_size), intent(out) :: wrlx(nbv,nbv)
  integer :: m

  wrlx = (1.0d0 - RELAX_ALPHA) * w
  do m = 1, nbv
    wrlx(m,m) = wrlx(m,m) + RELAX_ALPHA
  end do

  return
end subroutine weight_RTPP


!-----------------------------------------------------------------------
! Relaxation via LETKF weight - RTPS method
!-----------------------------------------------------------------------
subroutine weight_RTPS(w, pa, xb, wrlx, infl)
  implicit none
  real(r_size), intent(in) :: w(nbv,nbv)
  real(r_size), intent(in) :: pa(nbv,nbv)
  real(r_size), intent(in) :: xb(nbv)
  real(r_size), intent(out) :: wrlx(nbv,nbv)
  real(r_size), intent(out) :: infl
  real(r_size) :: var_g, var_a
  integer :: m, k

  var_g = 0.0d0
  var_a = 0.0d0
  do m = 1, nbv
    var_g = var_g + xb(m) * xb(m)
    do k = 1, nbv
      var_a = var_a + xb(k) * pa(k,m) * xb(m)
    end do
  end do
  if (var_g > 0.0d0 .and. var_a > 0.0d0) then
    infl = RELAX_ALPHA_SPREAD * sqrt(var_g / (var_a * real(nbv-1,r_size))) - RELAX_ALPHA_SPREAD + 1.0d0   ! Whitaker and Hamill 2012
!    infl = sqrt(RELAX_ALPHA_SPREAD * (var_g / (var_a * real(nbv-1,r_size))) - RELAX_ALPHA_SPREAD + 1.0d0) ! Hamrud et al. 2015 (slightly modified)
    wrlx = w * infl
  else
    wrlx = w
    infl = 1.0d0
  end if

  return
end subroutine weight_RTPS

!-----------------------------------------------------------------------
! AGREGADOS POR GIME
!-----------------------------------------------------------------------
! Data assimilation for observations: Compute analyses of observations (Y^a)
! * currently only support multiplicative and adaptive inflation
!  -- 01/01/2014, Guo-Yuan Lien,
! EFSO-GIME no la utiliza comentada en linea : 415 
!-----------------------------------------------------------------------
SUBROUTINE das_letkf_obs(v3dinfl,v2dinfl,logpfm,zfm)
  IMPLICIT NONE
  REAL(r_size),INTENT(IN) :: v3dinfl(nlon,nlat,nlev,nv3d)
  REAL(r_size),INTENT(IN) :: v2dinfl(nlon,nlat,nv2d)
  REAL(r_size),INTENT(IN) :: logpfm(nij1,nlev),zfm(nij1,nlev) ! agrego esto que
  ! se calcula en el das_letkf y es donde se llama esta subrutina
  REAL(r_size),ALLOCATABLE :: v3dinflx(:,:,:,:)
  REAL(r_size),ALLOCATABLE :: v2dinflx(:,:,:)
  !REAL(r_sngl),ALLOCATABLE :: v3d(:,:,:,:)
  !REAL(r_sngl),ALLOCATABLE :: v2d(:,:,:)
  !REAL(r_size),ALLOCATABLE :: v3dtmp(:,:,:)
  !REAL(r_size),ALLOCATABLE :: v2dtmp(:,:)
  !REAL(r_size),ALLOCATABLE :: tmpps(:)
  !REAL(r_size),ALLOCATABLE :: tmptv(:,:)
  !REAL(r_size),ALLOCATABLE :: tmpp(:,:)
  REAL(r_size),ALLOCATABLE :: obsanal(:,:)
  REAL(r_size),ALLOCATABLE :: obsanalmean(:)
  REAL(r_size) :: hdxf(nobstotal,nbv)
  REAL(r_size) :: rdiag(nobstotal)
  REAL(r_size) :: rloc(nobstotal)
  REAL(r_size) :: dep(nobstotal)
  REAL(r_size) :: ohx(nobs)
  REAL(r_size) :: parm
  REAL(r_size) :: trans(nbv,nbv)
  REAL(r_size) :: transrlx(nbv,nbv) 
  REAL(r_size) :: ri,rj,rk
  REAL(r_size) :: p_update_q, rlev
  INTEGER :: rlev2 ! lo pongo asi para poder usarlo en el obs_local
  REAL(r_size) :: q_sprd
  REAL(r_size) :: q_anal(nbv)
  REAL(r_size) :: z3d(nlon,nlat,nlev)     ! RADAR
!  REAL(r_size),ALLOCATABLE :: logpfm(:,:),zfm(:,:)
  INTEGER :: n,nn,m,k,nobsl,ierr,iret
  INTEGER :: ij,ilev,i,j ! G necesario para el DO del obs_local
  INTEGER :: inflelem,irank,nobsp,nobspmax
  CHARACTER(14) :: obsanalfile='obsanalNNN.dat'
  CHARACTER(9) :: gmean='guesNNNNN'

  WRITE(6,'(A)') 'Hello from das_letkf_obs: Compute [Y^a]'

  !
  ! If adaptive inflation is used, prepare a global array of inflation parameter
  !
  IF(cov_infl_mul <= 0.0d0) THEN
    ALLOCATE(v3dinflx(nlon,nlat,nlev,nv3d)) ! saco nv3dx
    ALLOCATE(v2dinflx(nlon,nlat,nv2d)) ! saco nv3dx
    IF(myrank == 0) THEN
      !ALLOCATE(v3dtmp(nlon,nlat,nlev,nv3d))
      !ALLOCATE(v2dtmp(nlon,nlat,nv2d))
      !ALLOCATE(tmpps(nlon*nlat))
      !ALLOCATE(tmptv(nlon*nlat,nlev))
      !ALLOCATE(tmpp(nlon*nlat,nlev))
      !CALL read_grd('gues_me.grd',v3dtmp,v2dtmp,0)  ! read ensemble mean into a temporary array
      ! esto lo tengo que cambiar para que lea nbv+1 del gues
      WRITE(gmean(5:9),'(I5.5)') nbv+1 
      CALL read_grd(gmean,1,v3dinflx,v2dinflx)  ! read ensemble mean into a temporary array asi es como lo pongo que esta en el SMN
      !CALL read_grdx('gues001.grd',v3dinflx,v2dinflx) ! only the orography is used, P will be recalulated
      v3dinflx(:,:,:,iv3d_u) = v3dinfl(:,:,:,iv3d_u)
      v3dinflx(:,:,:,iv3d_v) = v3dinfl(:,:,:,iv3d_v)
      v3dinflx(:,:,:,iv3d_t) = v3dinfl(:,:,:,iv3d_t)
      v3dinflx(:,:,:,iv3d_qs) = v3dinfl(:,:,:,iv3d_qs)
      v3dinflx(:,:,:,iv3d_qc) = v3dinfl(:,:,:,iv3d_qc)
      v2dinflx(:,:,iv2d_ps) = v2dinfl(:,:,iv2d_ps)
      !v2dinflx(:,:,iv2d_ps) = v3dinfl(:,:,1,iv3d_u) ! esto me parece muy raro que sea asi
      v3dinflx(:,:,:,iv3d_p) = v3dinfl(:,:,:,iv3d_p)
      !tmpps = reshape(v2dtmp(:,:,iv2d_ps),(/nlon*nlat/))
      !tmptv = reshape(v3dtmp(:,:,:,iv3d_t) * (1.0d0 + fvirt * v3dtmp(:,:,:,iv3d_q)),(/nlon*nlat,nlev/))
      !call sigio_modprd(nlon*nlat,nlon*nlat,nlev,gfs_nvcoord,gfs_idvc,gfs_idsl,&
      !                  gfs_vcoord,iret,tmpps,tmptv,pm=tmpp)
      !v3dinflx(:,:,:,iv3d_p) = reshape(tmpp,(/nlon,nlat,nlev/))
      !DEALLOCATE(v3dtmp,v2dtmp,tmpps,tmptv,tmpp)
    END IF
    CALL MPI_BARRIER(MPI_COMM_WORLD,ierr)
    call MPI_BCAST(v3dinflx,nlon*nlat*nlev*nv3d,MPI_r_size,0,MPI_COMM_WORLD,ierr)
    call MPI_BCAST(v2dinflx,nlon*nlat*nv2d,MPI_r_size,0,MPI_COMM_WORLD,ierr)
  END IF

  !
  ! Main LETKF loop
  !
  ALLOCATE(obsanal(nobs,nbv))
  ALLOCATE(obsanalmean(nobs))
  obsanal = 0.0d0
  obsanalmean = 0.0d0
  nn = myrank+1
  WRITE(6, *) 'nobs:', nobs
  DO   
    IF (nn > nobs) EXIT
    ! WRITE(6,'(A,I8)') 'nn = ',nn
    !
    ! The observation variable type is different from the grid variable type.
    ! To compute the analyses of observations as regular grids,
    ! what grid variable will the observation variable be regarded as?
    !
    ! Also determine the pressure level will the observation variable be
    ! regarded as?
    !
    ! ver de reemplazar por CALL get_iobs (obselm(nn), n)
    ! ver el parametro de inflacion es el correcto 
    SELECT CASE(NINT(obselm(nn)))
    CASE(id_u_obs)
      CALL get_iobs (NINT(obselm(nn)), n)      ! for variable localization, what grid variable to be regarded as? 
      inflelem = id_u_obs               ! for inflation parameter,   what grid variable to be regarded as?
      rlev = obslev(nn)
    CASE(id_v_obs)
      CALL get_iobs (NINT(obselm(nn)), n) 
      inflelem = id_v_obs               ! Es para p2k que necesita el numero de elem y para los IF
      rlev = obslev(nn)
    CASE(id_t_obs,id_tv_obs)
      CALL get_iobs (NINT(obselm(nn)), n) 
      inflelem = id_t_obs
      rlev = obslev(nn)
    CASE(id_qs_obs,id_rhs_obs)
      CALL get_iobs (NINT(obselm(nn)), n) 
      inflelem = id_qs_obs
      rlev = obslev(nn)
    CASE(id_ps_obs)
      CALL get_iobs (NINT(obselm(nn)), n) 
      !n = nv3d+iv2d_ps
      inflelem = id_ps_obs
      rlev = obsdat(nn)   ! for ps variable, use the observed pressure value
    CASE (id_reflectivity_obs)          ! RADAR
      CALL  get_iobs (NINT(obselm(nn)), n)     ! RADAR
      rlev = obslev(nn)                 ! RADAR
      inflelem = id_reflectivity_obs    ! RADAR
    CASE (id_radialwind_obs)            ! RADAR
      CALL  get_iobs (NINT(obselm(nn)), n)     ! RADAR
      rlev = obslev(nn)                 ! RADAR
      inflelem = id_radialwind_obs      ! RADAR
!    CASE(id_rain_obs)
!      n = 0
!      inflelem = id_q_obs
!      rlev = base_obsv_rain ! for precipitation, assigh the level 'base_obsv_rain'
    CASE DEFAULT
      n = 0
      IF(NINT(obselm(nn)) > 9999) THEN
        inflelem = id_ps_obs
        CALL itpl_2d(v3dinflx(:,:,1,iv3d_p),ri,rj,rlev)     
      ELSE
        inflelem = id_u_obs
        rlev = obslev(nn)
      END IF
    END SELECT
    !
    ! Determine the inflation parameter
    !
    IF(cov_infl_mul > 0.0d0) THEN
      parm = cov_infl_mul
    ELSE
      ! for radar observations 
      IF( inflelem == id_reflectivity_obs .or. inflelem == id_radialwind_obs )THEN      ! RADAR
        z3d= v3dinflx(:,:,:,iv3d_ph)/gg                                                 ! RADAR
        CALL z2k_fast(z3d,ri,rj,rlev,rk)                                                ! RADAR
      ELSE                                                                              ! RADAR
      !CALL phys2ijk(v3dinflx(:,:,:,iv3d_p),real(inflelem,r_size),obslon(nn),obslat(nn),rlev,ri,rj,rk)
        CALL p2k(v3dinflx(:,:,:,iv3d_p),real(inflelem,r_size),ri,rj,rlev,rk)    !G
      END IF
      IF(CEILING(rk) > nlev) THEN
        rk = REAL(nlev,r_size)
      END IF
      IF( CEILING(rk) < 2 .and. inflelem /= id_ps_obs )THEN
        IF( inflelem == id_us_obs .or. inflelem == id_vs_obs .or. inflelem == id_ts_obs .or. inflelem == id_qs_obs .or. inflelem == id_rhs_obs .or. &
            inflelem == id_u_obs  .or. inflelem == id_v_obs )THEN
          rk = 1.00001d0
        ENDIF
      ENDIF
!     REEMPLAZADO POR EL IF DE ARRIBA
!      IF(CEILING(rk) < 2 .AND. inflelem /= id_ps_obs) THEN
!        IF(inflelem > 9999) THEN
!          rk = 0.0d0
!        ELSE
!          rk = 1.00001d0
!        END IF
!      END IF
      IF(inflelem == id_ps_obs) THEN
        CALL itpl_2d(phi0,ri,rj,rk)  ! phi0 es ela orografia en SMN 
        rk = obslev(nn) - rk
      END IF
      !CALL Trans_XtoY(real(inflelem,r_size),ri,rj,rk,v3dinflx,v2dinflx,parm)
      ! QUEDA COMENTADO cuelga el LETKF. 
    END IF
    !
    ! LETKF computation
    !

    WRITE(6,*)'LETKF computation' 

    DO ilev=1,nlev              !G
       DO ij=1,nij1             !G
           ! n es cada iv3d_ que se obtuvo con get_iobs
           CALL obs_local(ij,ilev,n,hdxf,rdiag,rloc,dep,nobsl,logpfm,zfm)      !G y esto es como llama el obs_local el SMN 
           CALL letkf_core(nbv,nobstotal,nobsl,hdxf,rdiag,rloc,dep,parm,trans,minfl=MIN_INFL_MUL)
           CALL weight_RTPP(trans,transrlx)

          IF(ilev == 1) THEN !update 2d variable at ilev=1
           
             CALL obs_local(ij,ilev,n,hdxf,rdiag,rloc,dep,nobsl,logpfm,zfm) !G y esto es como llama el obs_local el SMN 
             CALL letkf_core(nbv,nobstotal,nobsl,hdxf,rdiag,rloc,dep,parm,trans,minfl=MIN_INFL_MUL)
             CALL weight_RTPP(trans,transrlx)

          END IF ! [ ilev == 1 ]
       END DO 
    END DO
    IF(n == iv3d_qv .OR. n == iv3d_qc) THEN
      CALL itpl_2d(v3dinflx(:,:,lev_update_q,iv3d_p),ri,rj,p_update_q)
    END IF
    IF((n == iv3d_qv .OR. n == iv3d_qc) .AND. obslev(nn) < p_update_q) THEN
      obsanal(nn,:) = obsdat(nn) - obsdep(nn) + obshdxf(nn,:)
      obsanalmean(nn) = obsdat(nn) - obsdep(nn)
    ELSE
      DO m=1,nbv
        obsanal(nn,m) = obsdat(nn) - obsdep(nn)
        DO k=1,nbv
          obsanal(nn,m) = obsanal(nn,m) + obshdxf(nn,k) * trans(k,m)
        END DO
        obsanalmean(nn) = obsanalmean(nn) + obsanal(nn,m)
      END DO
      obsanalmean(nn) = obsanalmean(nn) / real(nbv,r_size)
    END IF
    IF(n == iv3d_qs .AND. obslev(nn) >= p_update_q) THEN
      q_sprd = 0.0d0
      DO m=1,nbv
        q_anal(m) = obsanal(nn,m) - obsanalmean(nn)
        q_sprd = q_sprd + q_anal(m)**2
      END DO
      q_sprd = SQRT(q_sprd / REAL(nbv-1,r_size)) / obsanalmean(nn)
      IF(q_sprd > q_sprd_max) THEN
        DO m=1,nbv
          obsanal(nn,m) = obsanalmean(nn) + q_anal(m) * q_sprd_max / q_sprd
        END DO
      END IF
    END IF
    nn = nn + nprocs
  END DO
    
  !
  ! MPI_REDUCE and output obsanalfiles
  !
  ! mean
  CALL MPI_BARRIER(MPI_COMM_WORLD,ierr)
  CALL MPI_REDUCE(obsanalmean,ohx,nobs,MPI_r_size,MPI_SUM,0,MPI_COMM_WORLD,ierr)
  IF(myrank == 0) THEN
    WRITE(obsanalfile(8:10),'(A3)') '_me'
    WRITE(6,'(A,I3.3,2A)') 'MYRANK ',myrank,' is writing a file ',obsanalfile
    CALL write_obs2(obsanalfile,nobs,obselm,obslon,obslat,obslev, &
                    obsdat,obserr,obstyp,obsdif,ohx,obsqc,0)
  END IF
  ! members
  irank = 0
  DO m=1,nbv
    CALL MPI_BARRIER(MPI_COMM_WORLD,ierr)
    CALL MPI_REDUCE(obsanal(:,m),ohx,nobs,MPI_r_size,MPI_SUM,irank,MPI_COMM_WORLD,ierr)
    IF(myrank == irank) THEN
      WRITE(obsanalfile(8:10),'(I3.3)') m
      WRITE(6,'(A,I3.3,2A)') 'MYRANK ',myrank,' is writing a file ',obsanalfile
      CALL write_obs2(obsanalfile,nobs,obselm,obslon,obslat,obslev, &
                      obsdat,obserr,obstyp,obsdif,ohx,obsqc,0)
    END IF
    irank = irank + 1
    IF(irank >= nprocs) irank = 0
  END DO

  DEALLOCATE(obsanal)
  IF(cov_infl_mul <= 0.0d0) THEN
    DEALLOCATE(v3dinflx,v2dinflx)
  END IF
  RETURN
END SUBROUTINE das_letkf_obs
!-----------------------------------------------------------------------
! Subroutine for observation sensitivity computation
! Ported from Y.Ohta's SPEEDY-LETKF system by D.Hotta, 07/01/2013
! [ref: Eq.(6,7,9), Ota et al. 2013]
! Ver fcst3d,fcst2d, fcer3d, fcer2d salen de efso_tools
!-----------------------------------------------------------------------
! [INPUT]
!  gues3d,gues2d: xmean^g_0
!  fcst3d,fcst2d: C^(1/2)*X^f_t                    [(J/kg)^(1/2)]
!  fcer3d,fcer2d: C^(1/2)*[1/2(K-1)](e^f_t+e^g_t)  [(J/kg)^(1/2)]
! (save variables)
!  obshdxf:
! [OUTPUT]
!-----------------------------------------------------------------------
SUBROUTINE das_efso(gues3d,gues2d,fcst3d,fcst2d,fcer3d,fcer2d)
  IMPLICIT NONE
  REAL(r_size),INTENT(IN) :: gues3d(nij1,nlev,nv3d)     !
  REAL(r_size),INTENT(IN) :: gues2d(nij1,nv2d)          !
  REAL(r_size),INTENT(IN) :: fcst3d(nij1,nlev,nbv,nv3d) ! forecast ensemble
  REAL(r_size),INTENT(IN) :: fcst2d(nij1,nbv,nv2d)      !
  REAL(r_size),INTENT(IN) :: fcer3d(nij1,nlev,nv3d) ! forecast error
  REAL(r_size),INTENT(IN) :: fcer2d(nij1,nv2d)      !
  REAL(r_size),ALLOCATABLE :: hdxf(:,:)
  REAL(r_size),ALLOCATABLE :: hdxa_rinv(:,:)
  REAL(r_size),ALLOCATABLE :: rdiag(:)
  REAL(r_size),ALLOCATABLE :: rloc(:)
  REAL(r_size),ALLOCATABLE :: dep(:)
  REAL(r_size),ALLOCATABLE :: djdy(:,:)
  REAL(r_size),ALLOCATABLE :: recbuf(:,:)
  REAL(r_size) :: work1(nterm,nbv)
  INTEGER,ALLOCATABLE :: oindex(:)
  INTEGER :: ij,k,ilev,n,m,nob,nobsl,ierr,iret,iterm
  REAL(r_size),ALLOCATABLE :: logpfm(:,:),zfm(:,:)

  WRITE(6,'(A)') 'Hello from das_obsense'
  nobstotal = nobs_efso !+ ntvs G: cambio nobs por nobs_efso
  WRITE(6,'(A,I8)') 'Target observation numbers : NOBS_EFSO=', nobs_efso!,', NTVS=',ntvs 
  !
  ! In case of no obs
  !
  IF(nobstotal == 0) THEN
    WRITE(6,'(A)') 'No observation assimilated'
    RETURN
  END IF
  ALLOCATE(djdy(nterm,nobstotal))
  djdy = 0.0_r_size
  !
  ! p_full for background ensemble mean
  !
  ALLOCATE(logpfm(nij1,nlev),zfm(nij1,nlev))
  logpfm = DLOG(gues3d(:,:,iv3d_p))
  zfm    = gues3d(:,:,iv3d_ph)/gg  !Compute z in meters.
  !
  ! MAIN ASSIMILATION LOOP
  !
!$OMP PARALLEL PRIVATE(ij,ilev,k,hdxf,rdiag,rloc,dep,nobsl,oindex, &
!$                     work1,m,nob)
  ALLOCATE( hdxf(1:nobstotal,1:nbv),rdiag(1:nobstotal),rloc(1:nobstotal), &
       & dep(1:nobstotal) )
  ALLOCATE(oindex(1:nobstotal))
!--- For ILEV = 1 - NLEV
!$OMP DO SCHEDULE(DYNAMIC)
!! MUCHOS CAMBIOS CORRI TODOS ADENTRO DEL DO nv3d
  DO ilev=1,nlev
    WRITE(6,'(A,I3)') 'ilev = ',ilev
    DO ij=1,nij1
      IF(ABS(locadv_rate) > TINY(locadv_rate)) THEN
        CALL obs_local2(ij,ilev,0,hdxf,rdiag,rloc,dep,nobsl,logpfm,zfm,oindex) !G uso el obs_local2 que nvar=0
      ELSE
        CALL obs_local2(ij,ilev,0,hdxf,rdiag,rloc,dep,nobsl,logpfm,zfm,oindex) !G uso el obs_local2 que el nvar=0 
      END IF
      IF( nobsl /= 0 ) THEN
      ! Forecast error
        work1 = 0.0_r_size
        DO k=1,nv3d
          SELECT CASE(k)
          CASE(iv3d_u,iv3d_v)
            iterm = 1
          CASE(iv3d_t)
            iterm = 2
          CASE(iv3d_qv,iv3d_qc,iv3d_qr,iv3d_qci,iv3d_qg,iv3d_qs) ! Agrego todas las especies
            iterm = 3
          CASE DEFAULT
            iterm = 0
          END SELECT
          IF(iterm > 0) THEN
            DO m=1,nbv
              work1(iterm,m) = work1(iterm,m) + fcst3d(ij,ilev,m,k) * fcer3d(ij,ilev,k)
            END DO
          END IF
        END DO ! [ k=1,nv3d ] 
        IF(ilev == 1) THEN
          DO k=1,nv2d
            IF(k == iv2d_ps) THEN
              DO m=1,nbv
                work1(2,m) = work1(2,m) + fcst2d(ij,m,k) * fcer2d(ij,k)
              END DO  ! [ m=1,nbv]
            END IF ! (k == iv2d_ps) 
          END DO ! [ k=1, nv2d] 
        END IF ! (ilev == 1)  ! ver de usar END IF    
        !!! work1: [1/2(K-1)](X^f_t)^T*C*(e^f_t+e^g_t)  [J/kg]
        ! work1 es todo relacionado al modelo
        ! Hdxa Rinv
        ALLOCATE(hdxa_rinv(nobsl,nbv))
        DO m=1,nbv
          DO nob=1,nobsl
            hdxa_rinv(nob,m) = hdxf(nob,m) / rdiag(nob) * rloc(nob)
          END DO
        END DO
        !!! hdxa_rinv: rho*R^(-1)*Y^a_0 = rho*R^(-1)*(H X^a_0)
        ! dJ/dy
        ! aca mete las obs
        DO nob=1,nobsl
          DO m=1,nbv
            djdy(:,oindex(nob)) = djdy(:,oindex(nob)) + work1(:,m) * hdxa_rinv(nob,m)
          END DO
        END DO
        !!! djdy: [1/2(K-1)]rho*R^(-1)*Y^a_0*(X^f_t)^T*C*(e^f_t+e^g_t)
        DEALLOCATE(hdxa_rinv)
      END IF
    END DO ! [ ij=1,nij1 ]
  END DO ! [ ilev=1,nlev ]
!$OMP END DO
  DEALLOCATE(hdxf,rdiag,rloc,dep,oindex)
!$OMP END PARALLEL
  !
  ! Calculate observation sensitivity
  !
!$OMP PARALLEL PRIVATE(nob)
!$OMP DO
  DO nob=1,nobstotal
    WRITE(6,*) 'obsdat ', obsdat(nob)
    WRITE(6,*) 'obslev ', obslev(nob)
    WRITE(6,*) 'obslat ', obslat(nob),' obslon ', obslon(nob)
    WRITE(6,*) 'djdy ', djdy(:,nob)
    WRITE(6,*) 'obsdep ', obsdep(nob)
    obsense(:,nob) = djdy(:,nob) * obsdep(nob)
    WRITE(6,*) 'obsense ', obsense(:,nob) 
  END DO
  !!! obsense: delta e^{f-g}_t = [1/2(K-1)][y_o-H(xmean^b_0)]^T*rho*R^(-1)*Y^a_0*(X^f_t)^T*C*(e^f_t+e^g_t)
!$OMP END DO
!$OMP END PARALLEL
  ! Gather observation sensitivity informations to the root
  ALLOCATE(recbuf(nterm,nobstotal))
  CALL MPI_BARRIER(MPI_COMM_WORLD,ierr)
  CALL MPI_REDUCE(obsense(:,1:nobstotal),recbuf,nterm*nobstotal,MPI_r_size,MPI_SUM,0,MPI_COMM_WORLD,ierr)
  IF(myrank == 0) obsense(:,1:nobstotal) = recbuf(:,:)
  DEALLOCATE(recbuf)
  DEALLOCATE(djdy)
  DEALLOCATE(logpfm)
  DEALLOCATE(zfm)
  WRITE(6,*) 'Finish das_efso'
  RETURN
END SUBROUTINE das_efso


END MODULE letkf_tools
