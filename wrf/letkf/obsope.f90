PROGRAM obsope
!=======================================================================
!
! [PURPOSE:] Main program of observation operator
!
! [HISTORY:]
!   04/03/2013 Takemasa Miyoshi  created
!   08/30/2013 Guo-Yuan Lien     modified for GFS model
!   09/28/2013 Guo-Yuan Lien     add accumulated precipitation obs
!
!=======================================================================
  USE common
  USE common_wrf
  USE common_obs_wrf
  USE common_namelist
  USE map_utils

  IMPLICIT NONE
!  INTEGER,PARAMETER :: nslots=7 ! number of time slots for 4D-LETKF
!  INTEGER,PARAMETER :: nbslot=7 ! basetime slot
  REAL(r_size),PARAMETER :: minslot=60.0d0 ! time interval between slots in hour
  CHARACTER(11) :: obsinfile='obsTT.dat' !IN
  CHARACTER(11) :: obsinfile_mean='obsinmn.dat' !IN
  CHARACTER(10) :: guesfile='gsTT00001'   !IN formato SMN
!  CHARACTER(10) :: obsoutfile='obsout.dat' !OUT
  CHARACTER(12) :: obsoutfile='obsoutTT.dat' !OUT si quiero escribir para cada slot
  REAL(r_size),ALLOCATABLE :: elem(:)
  REAL(r_size),ALLOCATABLE :: rlon(:)
  REAL(r_size),ALLOCATABLE :: rlat(:)
  REAL(r_size),ALLOCATABLE :: rlev(:)
  REAL(r_size),ALLOCATABLE :: odat(:)
  REAL(r_size),ALLOCATABLE :: oerr(:)
  REAL(r_size),ALLOCATABLE :: otyp(:)
  REAL(r_size),ALLOCATABLE :: tdif(:)
  REAL(r_size),ALLOCATABLE :: ohx(:)
  REAL(r_size),ALLOCATABLE :: ri(:)
  REAL(r_size),ALLOCATABLE :: rj(:)
  INTEGER,ALLOCATABLE :: oqc(:)
  REAL(r_size),ALLOCATABLE :: v3d(:,:,:,:)
  REAL(r_size),ALLOCATABLE :: v2d(:,:,:)
!  REAL(r_size) :: pp(nlon,nlat)
!  REAL(r_size),PARAMETER :: threshold_dz=500.0d0
  REAL(r_size),ALLOCATABLE :: obsaz(:),obsel(:)
  REAL(r_size) :: dz
  INTEGER,ALLOCATABLE :: nobslots(:)
  INTEGER :: nobs,maxnobs
  REAL(r_size) :: rk
  INTEGER :: n,islot
  INTEGER :: l,im !G
!  REAL(r_size) :: obsaz,obsel
  LOGICAL :: firstwrite

  !Get configuration from namelist
  CALL read_namelist()
 
  CALL set_common_wrf('gs0100001')  ! Necesita un netcdf para leer las variables
                                    !(chequear luego si usamos este archivo u otro
  ALLOCATE( nobslots(nslots+1) )
  WRITE (6,*) 'NSLOTS: ', nslots
  DO islot=1,nslots
    !WRITE(6,*) islot, '--> NSLOT'
    !WRITE(obsinfile(6:7),'(I2.2)') islot
    WRITE(obsinfile(4:5),'(I2.2)') islot
    CALL get_nobs(obsinfile,nobslots(islot))
    WRITE(6,'(2A,I9,A)') obsinfile, ':', nobslots(islot), ' OBSERVATIONS'
  END DO
  CALL get_nobs(obsinfile_mean,nobslots(nslots+1))
  WRITE(6,'(2A,I9,A)') obsinfile_mean, ':', nobslots(nslots+1), ' OBSERVATIONS'
  nobs = SUM(nobslots)
  maxnobs = MAXVAL(nobslots)
  WRITE(6,'(A,I9,A)') 'TOTAL:      ', nobs, ' OBSERVATIONS'
  WRITE(6,'(A,I9,A)') 'MAX:      ', maxnobs, ' OBSERVATIONS'
  ALLOCATE( elem(maxnobs) )
  ALLOCATE( rlon(maxnobs) )
  ALLOCATE( rlat(maxnobs) )
  ALLOCATE( rlev(maxnobs) )
  ALLOCATE( odat(maxnobs) )
  ALLOCATE( oerr(maxnobs) )
  ALLOCATE( otyp(maxnobs) )
  ALLOCATE( tdif(maxnobs) )
  ALLOCATE( ohx(maxnobs) )
  ALLOCATE( oqc(maxnobs) )
  ALLOCATE( ri(maxnobs) )
  ALLOCATE( rj(maxnobs) )
  ALLOCATE( obsaz(maxnobs) ) ! son de radar
  ALLOCATE( obsel(maxnobs) ) ! son de radar
  ALLOCATE(v3d(nlon,nlat,nlev,nv3d))
  ALLOCATE(v2d(nlon,nlat,nv2d))
  firstwrite = .TRUE.
  !pp = 0.0d0
  !obsaz = 0.0d0 ! cosas de radar los pongo en 0 
  !obsel = 0.0d0 ! cosas de radar los pongo en 0 
  DO islot=1,nslots
    !WRITE(*,*) 'firstwrite:', firstwrite
    IF(nobslots(islot) == 0) CYCLE
    WRITE(obsinfile(4:5),'(I2.2)') islot
    WRITE(6,*) 'Lee el archivo: ', obsinfile
    CALL read_obs(obsinfile,nobslots(islot),&
      & elem(1:nobslots(islot)),rlon(1:nobslots(islot)),&
      & rlat(1:nobslots(islot)),rlev(1:nobslots(islot)),&
      & odat(1:nobslots(islot)),oerr(1:nobslots(islot)),&
      & otyp(1:nobslots(islot)))
    tdif(1:nobslots(islot)) = REAL(islot-nbslot,r_size)*minslot ! esta en min
    DO n=1,nobslots(islot)
      CALL latlon_to_ij(projection,rlat(n),rlon(n),ri(n),rj(n))
    END DO 
    WRITE(guesfile(3:4),'(I2.2)') islot
    
    CALL read_grd(guesfile,1,v3d,v2d) !gues en netcdf
    WRITE(6,*) 'Lee el archivo: ', guesfile
!    if (islot == 1 .or. islot == nslots) then
!      pp = pp + 0.5d0 * hourslot * v2d(:,:,iv2d_tprcp)
!    else
!      pp = pp + hourslot * v2d(:,:,iv2d_tprcp)
!    end if
    ! inicializo las variables en 0. 
    ohx=0.0d0
    oqc=0
    WRITE(6,*) 'nobslot : ', nobslots(islot)
    DO n=1,nobslots(islot)
!      WRITE(6,*) 'n : ', n
!     CALL phys2ijk(v3d(:,:,:,iv3d_p),elem(n),rlon(n),rlat(n),rlev(n),ri,rj,rk)
      IF(CEILING(ri(n)) < 2 .OR. nlon-1 < CEILING(ri(n))) THEN
!        WRITE(6,'(A)') '* X-coordinate out of range'
!        WRITE(6,'(A,F6.2,A,F6.2)') '*   ri=',ri,', rlon=',rlon(n)
        CYCLE
      END IF
      IF(CEILING(rj(n)) < 2 .OR. nlat-1 < CEILING(rj(n))) THEN
!        WRITE(6,'(A)') '* Y-coordinate out of range'
!        WRITE(6,'(A,F6.2,A,F6.2)') '*   rj=',rj,', rlat=',rlat(n)
        CYCLE
      END IF
      CALL p2k(v3d(:,:,:,iv3d_p),elem(n),ri(n),rj(n),rlev(n),rk)
      IF(CEILING(rk) > nlev-1) THEN
!        CALL itpl_2d(v2d(:,:,iv2d_orog),ri,rj,dz)
!        WRITE(6,'(A)') '* Z-coordinate out of range'
!        WRITE(6,'(A,F6.2,A,F10.2,A,F6.2,A,F6.2,A,F10.2)') &
!         & '*   rk=',rk,', rlev=',rlev(n),&
!         & ', (lon,lat)=(',rlon(n),',',rlat(n),'), phi0=',dz
        CYCLE
      END IF
        IF(CEILING(rk) < 2 .AND. NINT(elem(n)) /= id_ps_obs) THEN
          IF(NINT(elem(n)) > 9999) THEN
            rk = 0.0d0
        ELSE IF(NINT(elem(n)) == id_u_obs .OR. NINT(elem(n)) == id_v_obs) THEN
          rk = 1.00001d0
        ELSE
!          CALL itpl_2d(v2d(:,:,iv2d_orog),ri,rj,dz)
!          WRITE(6,'(A)') '* Z-coordinate out of range'
!          WRITE(6,'(A,F6.2,A,F10.2,A,F6.2,A,F6.2,A,F10.2)') &
!           & '*   rk=',rk,', rlev=',rlev(n),&
!           & ', (lon,lat)=(',rlon(n),',',rlat(n),'), phi0=',dz
          CYCLE
        END IF
      END IF
      IF(NINT(elem(n)) == id_ps_obs .AND. odat(n) < -100.0d0) CYCLE
      IF(NINT(elem(n)) == id_ps_obs) THEN
        CALL itpl_2d(phi0,ri(n),rj(n),dz) !phi0 orog del SMN
        rk = rlev(n) - dz
        IF(ABS(rk) > threshold_dz) THEN ! pressure adjustment threshold
!          WRITE(6,'(A)') '* PS obs vertical adjustment beyond threshold'
!          WRITE(6,'(A,F10.2,A,F6.2,A,F6.2,A)') '*   dz=',rk,&
!           & ', (lon,lat)=(',elon(n),',',elat(n),')'
          CYCLE
        END IF
      END IF
      IF(NINT(elem(n)) == id_ts_obs .or. NINT(elem(n)) == id_qs_obs .or. NINT(elem(n)) == id_rhs_obs .or. &
         NINT(elem(n)) == id_us_obs .or. NINT(elem(n)) == id_vs_obs ) THEN
         CALL itpl_2d(phi0,ri(n),rj(n),dz)
         dz = rlev(n) - dz
         rk = 1.00001d0
         IF( ABS( dz ) > 200.0 )THEN
           WRITE(6,*)'Assimilation of surface observation fails, delta z is too high'
           CYCLE
         ENDIF
      ENDIF
      !
      ! observational operator
      !
      ! comento que no calcule el Trans_XtoY ya qye lo hace en el set_letkf_obs
      ! (letkf_obs.f90) y ohx esta incilizado en 0. 
      !CALL Trans_XtoY(elem(n),ri,rj,rk,v3d,v2d,ohx(n))
      !CALL Trans_XtoY(elem(n),otyp(n),rlon(n),rlat(n),odat(n),ri(n),rj(n),rk,obsaz(n),obsel(n),v3d,v2d,ohx(n))
      oqc(n) = 1
    END DO
    WRITE(obsoutfile(7:8),'(I2.2)') islot
    WRITE(*,*) 'obsoutfile: ', obsoutfile
    CALL write_obs2(obsoutfile,nobslots(islot),&
      & elem(1:nobslots(islot)),rlon(1:nobslots(islot)),&
      & rlat(1:nobslots(islot)),rlev(1:nobslots(islot)),&
      & odat(1:nobslots(islot)),oerr(1:nobslots(islot)),&
      & otyp(1:nobslots(islot)),tdif(1:nobslots(islot)),&
      & ohx(1:nobslots(islot)),oqc(1:nobslots(islot)),0)
!    IF(firstwrite) THEN
!      CALL write_obs2(obsoutfile,nobslots(islot),&
!        & elem(1:nobslots(islot)),rlon(1:nobslots(islot)),&
!        & rlat(1:nobslots(islot)),rlev(1:nobslots(islot)),&
!        & odat(1:nobslots(islot)),oerr(1:nobslots(islot)),&
!        & otyp(1:nobslots(islot)),tdif(1:nobslots(islot)),&
!        & ohx(1:nobslots(islot)),oqc(1:nobslots(islot)),0)
!      firstwrite = .FALSE.
!    ELSE
!      WRITE(6,*) 'Escribe sobre el obsout.dat con append'
!      CALL write_obs2(obsoutfile,nobslots(islot),&
!        & elem(1:nobslots(islot)),rlon(1:nobslots(islot)),&
!        & rlat(1:nobslots(islot)),rlev(1:nobslots(islot)),&
!        & odat(1:nobslots(islot)),oerr(1:nobslots(islot)),&
!        & otyp(1:nobslots(islot)),tdif(1:nobslots(islot)),&
!        & ohx(1:nobslots(islot)),oqc(1:nobslots(islot)),1)
!    END IF
  END DO
! este caso no estoy segura cual seria ? 
  IF(nobslots(nslots+1) > 0) THEN
!    v2d(:,:,iv2d_tprcp) = pp
    CALL read_obs(obsinfile_mean,nobslots(nslots+1),&
      & elem(1:nobslots(nslots+1)),rlon(1:nobslots(nslots+1)),&
      & rlat(1:nobslots(nslots+1)),rlev(1:nobslots(nslots+1)),&
      & odat(1:nobslots(nslots+1)),oerr(1:nobslots(nslots+1)),&
      & otyp(1:nobslots(nslots+1)))
    tdif(1:nobslots(nslots+1)) = 0.0d0
    ohx=0.0d0
    oqc=0

    DO n=1,nobslots(nslots+1)
      !IF(elem(n) /= id_rain_obs) CYCLE ! si no es distinto de rain continua 
      !CALL phys2ij(rlon(n),rlat(n),ri,rj) ! esto entiendo que es para lluvia
      IF(CEILING(ri(n)) < 2 .OR. nlon+1 < CEILING(ri(n))) THEN
!        WRITE(6,'(A)') '* X-coordinate out of range'
!        WRITE(6,'(A,F6.2,A,F6.2)') '*   ri=',ri,', rlon=',rlon(n)
        CYCLE
      END IF
      IF(CEILING(rj(n)) < 2 .OR. nlat < CEILING(rj(n))) THEN
!        WRITE(6,'(A)') '* Y-coordinate out of range'
!       WRITE(6,'(A,F6.2,A,F6.2)') '*   rj=',rj,', rlat=',rlat(n)
        CYCLE
      END IF
      !
      ! observational operator
      !
      rk = 0.0d0
      !CALL Trans_XtoY(elem(n),otyp(n),rlon(n),rlat(n),odat(n),ri(n),rj(n),rk,obsaz(n),obsel(n),v3d,v2d,ohx(n))
      oqc(n) = 1
    END DO
    IF(firstwrite) THEN
      CALL write_obs2(obsoutfile,nobslots(nslots+1),&
        & elem(1:nobslots(nslots+1)),rlon(1:nobslots(nslots+1)),&
        & rlat(1:nobslots(nslots+1)),rlev(1:nobslots(nslots+1)),&
        & odat(1:nobslots(nslots+1)),oerr(1:nobslots(nslots+1)),&
        & otyp(1:nobslots(nslots+1)),tdif(1:nobslots(nslots+1)),&
        & ohx(1:nobslots(nslots+1)),oqc(1:nobslots(nslots+1)),0)
      firstwrite = .FALSE.
    ELSE
      CALL write_obs2(obsoutfile,nobslots(nslots+1),&
        & elem(1:nobslots(nslots+1)),rlon(1:nobslots(nslots+1)),&
        & rlat(1:nobslots(nslots+1)),rlev(1:nobslots(nslots+1)),&
        & odat(1:nobslots(nslots+1)),oerr(1:nobslots(nslots+1)),&
        & otyp(1:nobslots(nslots+1)),tdif(1:nobslots(nslots+1)),&
        & ohx(1:nobslots(nslots+1)),oqc(1:nobslots(nslots+1)),1)
    END IF
  END IF

  DEALLOCATE( elem,rlon,rlat,rlev,odat,oerr,otyp,tdif,ohx,oqc,v3d,v2d )

END PROGRAM obsope
